source("util.R")
source("bug_prediction_tuning.R")
source("bug_prediction.R")

##
# Get data

tuning_tournament_ranking <- tournament_ranking(5)
tuning_subjects <- get_tuning_subjects()

##
# Create new file

MACROS_FILE = paste(PAPER_DIRECTORY_PATH, "/macros.tex", sep="")
unlink(MACROS_FILE)
sink(MACROS_FILE, append=FALSE, split=TRUE)

#cat("% Macros file generated on the ", date(), "\n", sep="")
cat("% Macros file\n", sep="")

##
# Apps versions

cat("\n% Apps versions\n", sep="")
print_macro("dfourjVersion", "v1.1.0")

##
# Tuning macros

cat("\n% Tuning macros\n", sep="")
print_macro("tuningNumProjects", unique(tuning_subjects$"num_projects"))
print_macro("tuningNumBugsPerProject", unique(tuning_subjects$"num_bugs_per_project"))
print_macro("tuningTotalNumSubjects", length(tuning_subjects$"bugs"))
print_macro("tuningSubjects", as.vector(tuning_subjects$"bugs"))
print_macro("tuningNumConfs", unique(tuning_subjects$"total_num_configurations"))
print_macro("tuningNumValidConfs", unique(tuning_subjects$"total_valid_configurations"))
print_macro("tuningNumSchwaExecs", unique(tuning_subjects$"total_num_executions"))
print_macro("tuningBestRW", tuning_tournament_ranking$"rw")
print_macro("tuningBestFW", tuning_tournament_ranking$"fw")
print_macro("tuningBestAW", tuning_tournament_ranking$"aw")
print_macro("tuningBestTR", tuning_tournament_ranking$"tr")
print_macro("tuningBestPos", tuning_tournament_ranking$"position")
print_macro("tuningBestScore", tuning_tournament_ranking$"score")
print_macro("tuningBestBetterThan", tuning_tournament_ranking$"better_than")
print_macro("tuningBestOutOf", tuning_tournament_ranking$"num_confs")
print_macro("tuningBestEffectSize", tuning_tournament_ranking$"effect_size")

##
# Close file
sink()

# EOF

