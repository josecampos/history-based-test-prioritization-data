source("util.R")

##
#
##
overall_boxplot <- function(df, boxplot_name) {

  library(ggplot2)
  library(dplyr)
  library(tidyr)
  library(purrr)
  # TODO ideally we would like to use their pretty names, i.e.,
  # pretty_print_algorithm_name(algorithm)

  df <- df %>% mutate(algorithm = purrr::map(algorithm, function(x)strsplit(pretty_print_algorithm_name(x, FALSE), "\\\\")[[1]][1]))
  # order algorithms
  df <- df %>% mutate(algorithm = factor(algorithm, levels=c("Random","Greedy", "Add. Greedy", "GA", "Rand. Search", 
                                                             "AFSAC", "Elbaum et al.", "MCCTCP", "ROCKET", "G-clef")))
  # in the case of multiple repetitions, take the average position of the first trigger test
  df <- df %>% 
    group_by(project,bug_id,algorithm) %>% 
    summarise(average = mean(first/num_test_cases))
  p <- df %>% ggplot(aes(x=algorithm, y=average)) + 
    geom_boxplot() + 
    labs(x="Strategy", y="% of test cases that have to be\nexecuted to trigger the faulty behaviour") + 
    stat_summary(fun.y=mean, colour="red", geom="point", size=2, shape=8, show.legend=F) +
    facet_wrap(~project)

  pdfname <- paste(PAPER_DIRECTORY_PATH, "/figs/", boxplot_name, sep="")
  unlink(pdfname)
  pdf(file=pdfname, family="Helvetica", width=9, height=5)

  plot <- p + theme_bw() + theme(axis.text.x = element_text(angle = 45, vjust=1,hjust=1))
  print(plot)

  dev.off()
  embed_fonts(pdfname, options="-dSubsetFonts=true -dEmbedAllFonts=true -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dMaxSubsetPct=100")
}

##
#
##
make_table_content <- function(df, projects, ranking_df, rank_pos="first") {

  algorithms <- unique(df$"algorithm")
  for (algorithm in algorithms) {
    algorithm_mask <- df$"algorithm" == algorithm

    for (secondary_objective in unique(df$"secondary_objective"[algorithm_mask])) {
      secondary_objective_mask <- df$"secondary_objective" == secondary_objective

      for (classes_per_group in unique(df$"classes_per_group"[algorithm_mask & secondary_objective_mask])) {
        classes_per_group_mask <- df$"classes_per_group" == classes_per_group

        cat(pretty_print_algorithm_name(algorithm), sep="")
        #cat(" & ", pretty_print_secondary_objective(secondary_objective), " & ", classes_per_group, sep="")
        #cat(pretty_print_secondary_objective(secondary_objective), " & ", classes_per_group, sep="")
        #if (classes_per_group > 1) {
        #  cat("\\%", sep="")
        #}

        overall_num_tests <- c()
        overall_num_tests_after_sorting <- c()
        overall_ranking <- c()

        for (project in projects) {
          project_mask <- df$"project" == project

          num_tests <- c()
          num_tests_after_sorting <- c()

          for (bug_id in unique(df$"bug_id"[project_mask])) {
            bug_id_mask <- df$"bug_id" == bug_id

            total_num_tests <- unique(df$"num_test_cases"[algorithm_mask & secondary_objective_mask & classes_per_group_mask & project_mask & bug_id_mask])
            if (length(total_num_tests) == 0) {
              next
            }

            num_tests <- c(num_tests, total_num_tests)
            num_tests_after_sorting <- c(num_tests_after_sorting, mean(df[[rank_pos]][algorithm_mask & secondary_objective_mask & classes_per_group_mask & project_mask & bug_id_mask]))
          }

          overall_num_tests <- c(overall_num_tests, mean(num_tests))
          overall_num_tests_after_sorting <- c(overall_num_tests_after_sorting, mean(num_tests_after_sorting))

          percent_num_tests_after_sorting <- mean(num_tests_after_sorting) / mean(num_tests) * 100
          cat(" & ", sprintf("%.1f", round(mean(num_tests_after_sorting), 1)), " (", sprintf("%.1f", round(percent_num_tests_after_sorting, 1)), "\\%)", sep="")
          # ranking
          rank <- as.numeric(ranking_df$"rank"[ranking_df$"algorithm" == algorithm &
                                               ranking_df$"secondary_objective" == secondary_objective &
                                               ranking_df$"classes_per_group" == classes_per_group &
                                               ranking_df$"project" == project])
          overall_ranking <- c(overall_ranking, rank)
          cat(" & ", sprintf("%.1f", round(mean(rank), 1)), sep="")
        }

        overall_percent_num_tests_after_sorting <- mean(overall_num_tests_after_sorting) / mean(overall_num_tests) * 100
        cat(" & ", sprintf("%.1f", round(mean(overall_num_tests_after_sorting), 1)), " (", sprintf("%.1f", round(overall_percent_num_tests_after_sorting, 1)), "\\%)", sep="")
        cat(" & ", sprintf("%.1f", round(mean(overall_ranking), 1)), sep="")

        cat(" \\\\", "\n", sep="")
      }
    }
  }
}

##
#
##
overall_table <- function(df, table_name, rank_pos="first") {

  projects <- unique(df$"project")

  ranking_df <- perform_ranking_analysis(df, projects, rank_pos)
  friedman_test_nonperfect_bug_prediction <- perform_friedman_test(ranking_df)

  ##
  # create tex file
  LATEX_TABLE = paste(PAPER_DIRECTORY_PATH, "/tables/", table_name, sep="")
  unlink(LATEX_TABLE)
  sink(LATEX_TABLE, append=FALSE, split=TRUE)

  ##
  # header
  cat("\\begin{tabular}{@{\\extracolsep{\\fill}} l | ", sep="")
  for (i in 1:length(projects)){
    cat("cc", sep="")
  }
  cat(" | cc}\\toprule", "\n", sep="")
  for (project in projects) {
    cat(" & \\multicolumn{2}{c}{", project, "}", sep="")
  }
  cat(" & \\multicolumn{2}{c}{Overall} \\\\", "\n", sep="") # Overall
#  cat("\\midrule", "\n", sep="")
  cat("Strategy ", sep="")
  for (project in projects) {
    cat(" & \\#t & R ", sep="")
  }
  cat(" & \\#t & R \\\\", "\n", sep="") # Overall

  ## non-perfect bug prediction
  cat("\\midrule", "\n", sep="")
  cat("%", "\n", sep="")
  cat("\\rowcolor{gray!25}", "\n", sep="")
  cat("\\multicolumn{15}{c}{\\textbf{\\textit{", # FIXME 15
        "$\\chi^2=", sprintf("%.2f", round(friedman_test_nonperfect_bug_prediction$"statistic", 2)), "$",
        ", $p-value ", pretty_print_p_value(friedman_test_nonperfect_bug_prediction$"p.value"), "$", "}}} \\\\", "\n", sep="")
  make_table_content(df, projects, ranking_df, rank_pos)

  ##
  # footer
  cat("\\bottomrule", "\n", sep="")
  cat("\\end{tabular}", "\n", sep="")

  sink()
}

##
# Schwa + Coverage-based
confs <- data.frame(
  algorithm = c("greedy", "additionalgreedy", "random", "randomsearch", "geneticalgorithm", "schwa"),
  secondary_objective = c(rep("none", 5), "constraint_solver"),
  classes_per_group = c(rep(1, 5), 1),
  stringsAsFactors = FALSE
)
df <- filter_data(
        filter_tuning_subjects(
          filter_configurations(
            read.csv("../data/test_case_prioritisation_data.csv", header=T, stringsAsFactors=F), confs, FALSE
          )
        )
      )
overall_table(df, "overall_schwa_and_coverage_based.tex", "first")
overall_boxplot(df, "overall_schwa_and_coverage_based.pdf")

##
# Schwa + History-based
confs <- data.frame(
  algorithm = c("cho", "elbaum", "huang", "marijan", "schwa"),
  secondary_objective = c(rep("none", 4), "constraint_solver"),
  classes_per_group = c(rep(1, 4), 1),
  stringsAsFactors = FALSE
)
df <- filter_data(
        filter_tuning_subjects(
          filter_configurations(
            filter_data_number_of_occurences_of_triggering_tests("../data/test_case_prioritisation_data.csv"), confs, FALSE
          )
        )
      )
overall_table(df, "overall_schwa_and_history_based.tex", "first")
overall_boxplot(df, "overall_schwa_and_history_based.pdf")

# EOF
