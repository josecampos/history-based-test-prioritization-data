source("util.R")

library("hash")

DATA_FILE="../experiments/bug_prediction/bug_prediction_tuning.csv.gz"
SIMPLE_DATA_FILE="../experiments/bug_prediction/bug_prediction_tuning_simple.csv.gz"
D4J_PROJECTS_HOME="../tools/defects4j/framework/projects"

rws <- c(0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1)
fws <- c(0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1)
aws <- c(0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1)
trs <- c(0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1)

# ----------------------------------------------------- util functions

##
#
##
is_valid_sum_of_weights <- function(revision, fix, author) {
  sum_features_weights = round(revision + fix + author, 10)
  if (sum_features_weights != 1.0) {
    return(FALSE)
  }
  return(TRUE)
}

##
#
##
simplify_data_frame <- function() {

  cat("Loading data ...\n", sep="")
  df <- load_TABLE(DATA_FILE)
  cat("done.\n", sep="")

  projects <- unique(df$"project_name")

  simple_df <- data.frame()

  for (revisions_weight in rws) {
    for (fixes_weight in fws) {
      for (authors_weight in aws) {
        if (is_valid_sum_of_weights(revisions_weight, fixes_weight, authors_weight) == FALSE) {
          next;
        }

        for (time_range in trs) {
          df_rev <- df[which(df$"revisions_weight" == revisions_weight &
                        df$"fixes_weight" == fixes_weight &
                        df$"authors_weight" == authors_weight &
                        df$"time_range" == time_range), ]

          for (project_name in projects) {
            for (bug_id in unique(df_rev$"bug_id"[df_rev$"project_name" == project_name])) {

              ## get buggy classes names
              buggy_classes_file = paste(D4J_PROJECTS_HOME, "/", project_name,
                              "/modified_classes/", bug_id, ".src", sep="")
              list_of_buggy_classes <- read.csv(buggy_classes_file, header=F, stringsAsFactors=F)

              ## get position of the first buggy class
              dt_bug <- df_rev[which(df_rev$"project_name" == project_name & df_rev$"bug_id" == bug_id), ]

              ranking_positions <- c()
              for (elem in list_of_buggy_classes$"V1") {
                ranking_positions <- c(ranking_positions, which(dt_bug$"class_name" == elem))
              }
              stopifnot(length(ranking_positions) != 0)

              position = min(ranking_positions)
              score = min(ranking_positions) / length(dt_bug$"class_name")

              row <- data.frame(pid=project_name, bid=bug_id,
                                rw=revisions_weight,
                                fw=fixes_weight,
                                aw=authors_weight,
                                tr=time_range,
                                position=position,
                                score=score)
              simple_df <- rbind(simple_df, row)
            }
          }
        }
      }
    }
  }

  cat("Data is loaded and processed. Starting compressing. ", date(), "\n")
  write.table(simple_df, file = gzfile(SIMPLE_DATA_FILE))
  cat("Data is compressed and saved. Starting reading back. ", date(), "\n")
  table <- read.table(gzfile(SIMPLE_DATA_FILE), header=T, stringsAsFactors=F)
  cat("Data read back. Done! ", date(),"\n")
}

# ----------------------------------------------------- main functions

##
#
##
get_tuning_subjects <- function() {

  dt <- load_TABLE(SIMPLE_DATA_FILE)

  projects <- sort(unique(dt$"pid"))
  num_projects <- length(projects)

  bugs <- c()
  num_bugs_per_project = 0
  for (pid in projects) {
    for (bid in sort(unique(dt$"bid"[dt$"pid" == pid]))) {
      if (num_bugs_per_project == 0) {
        # assuming we have same number of bugs per project
        num_bugs_per_project = length(unique(dt$"bid"[dt$"pid" == pid]))
      }

      bugs <- c(bugs, paste(pid, "-", bid, sep=""))
    }
  }

  total_num_configurations = 0
  total_valid_configurations = 0
  for (revisions_weight in rws) {
    for (fixes_weight in fws) {
      for (authors_weight in aws) {
        total_num_configurations = total_num_configurations + 10 # taking into account TR
        if (is_valid_sum_of_weights(revisions_weight, fixes_weight, authors_weight) == FALSE) {
          next;
        }

        for (time_range in trs) {
          total_valid_configurations = total_valid_configurations + 1
        }
      }
    }
  }

  total_num_executions = num_projects * num_bugs_per_project * total_valid_configurations
  return(data.frame(num_projects=num_projects, num_bugs_per_project=num_bugs_per_project,
    bugs=bugs, total_num_configurations=total_num_configurations,
    total_valid_configurations=total_valid_configurations,
    total_num_executions=total_num_executions))
}

##
#
##
tournament_ranking <- function(best_N=3, worst_N=3) {

  dt <- load_TABLE(SIMPLE_DATA_FILE)

  ##
  # Cache data so that we do not need to compute scores all the times
  data_cache <- hash()

  cat("Ranking configurations ...\n", sep="")
  ranking <- data.frame()
  num_confs = 0

  for (revisions_weight in rws) {
    for (fixes_weight in fws) {
      for (authors_weight in aws) {
        if (is_valid_sum_of_weights(revisions_weight, fixes_weight, authors_weight) == FALSE) {
          next;
        }

        for (time_range in trs) {
          num_confs = num_confs + 1

          configuration_name = paste("rev-", revisions_weight, "_fix-", fixes_weight, "_aut-", authors_weight, "_tr-", time_range, sep="")
          print(configuration_name)
          if (has.key(configuration_name, data_cache) == FALSE) {
            data_cache[[configuration_name]] <- dt[which(dt$"rw" == revisions_weight &
                                                         dt$"fw" == fixes_weight &
                                                         dt$"aw" == authors_weight &
                                                         dt$"tr" == time_range), ]
          }
          configuration_positions <- data_cache[[configuration_name]]$"position"
          configuration_scores <- data_cache[[configuration_name]]$"score"
          stopifnot(length(configuration_positions) != 0)

          ## tournament ranking: comparing the sets of positions/scores
          # of each pair of techniques, awarding 1 point to the winner
          # if it is statistically significantly better

          effect_sizes <- c()
          points = 0
          for (rw in rws[rws != revisions_weight]) {
            for (fw in fws[fws != fixes_weight]) {
              for (aw in aws[aws != authors_weight]) {
                if (is_valid_sum_of_weights(rw, fw, aw) == FALSE) {
                  next;
                }

                for (tr in trs[trs != time_range]) {
                  stopifnot(revisions_weight != rw && fixes_weight != fw && authors_weight != aw && time_range != tr)

                  cn = paste("rev-", rw, "_fix-", fw, "_aut-", aw, "_tr-", tr, sep="")
                  if (has.key(cn, data_cache) == FALSE) {
                    data_cache[[cn]] <- dt[which(dt$"rw" == rw &
                                                 dt$"fw" == fw &
                                                 dt$"aw" == aw &
                                                 dt$"tr" == tr), ]
                  }
                  positions <- data_cache[[cn]]$"position"
                  scores <- data_cache[[cn]]$"score"
                  stopifnot(length(positions) != 0)

                  ##
                  # did rw-fw-aw outperformed 'configuration_name'?

#                  #t_test <- t.test(configuration_scores, scores, paired=TRUE)
#                  t_test <- t.test(configuration_positions, positions, paired=TRUE)
#                  p <- t_test$p.value
#                  if (p < 0.05 && t_test$estimate < 0) {
#                    points = points + 1
#                    #effect_size <- cohen.d(configuration_scores, scores, paired=TRUE)$estimate
#                    effect_size <- cohen.d(configuration_positions, positions, paired=TRUE)$estimate
#                    effect_sizes <- c(effect_sizes, effect_size)
#                  } else {
#                    points = points - 1
#                  }

                  a12 <- A12(configuration_positions, positions)
                  w <- wilcox.test(configuration_positions, positions, exact=FALSE, paired=FALSE)
                  p <- w$p.value
                  if (p < 0.05 && a12 < 0.5) {
                    # when A12 is more > than 0.5, the first technique is worse
                    # when A12 is less < than 0.5, the second technique is worse
                    points = points + 1
                    effect_sizes <- c(effect_sizes, a12)
                  } else {
                    points = points - 1
                  }
                }
              }
            }
          }

          mean_effect_size = NA
          if (length(effect_sizes) > 0) {
            mean_effect_size = mean(effect_sizes)
          }

          configuration_positions_sd <- sd(configuration_positions)
          configuration_positions_ci <- get_ci(configuration_positions)
          configuration_positions_ci_min <- configuration_positions_ci[1]
          configuration_positions_ci_max <- configuration_positions_ci[2]

          r <- data.frame(rw=revisions_weight, fw=fixes_weight, aw=authors_weight, tr=time_range,
                          mean_position=mean(configuration_positions), mean_score=mean(configuration_scores),
                          configuration_positions_sd=configuration_positions_sd,
                          configuration_positions_ci_min=configuration_positions_ci_min, configuration_positions_ci_max=configuration_positions_ci_max,
                          points=points, mean_effect_size=mean_effect_size)
          ranking <- rbind(ranking, r)
        }
      }
    }
  }
  cat("done.\n", sep="")
  stopifnot(num_confs == nrow(ranking))

  # sort 'ranking' data frame by 'points'
  ranking <- ranking[order(ranking$points, -ranking$mean_position, -ranking$mean_effect_size, decreasing=TRUE), ]

  print("=== Ranking ===")
  print(ranking)

  # print a table of the best N, e.g., 5
  print("=== Latex Table ===")
  best_ranking <- head(ranking, best_N)
  worst_ranking <- tail(ranking, worst_N)

  does_best_perform_sig_better <- TRUE
  if (is.na(head(best_ranking, 1)$"mean_effect_size")) {
    does_best_perform_sig_better <- FALSE
  }

  TABLE = paste(PAPER_DIRECTORY_PATH, "/tables/bug_prediction_tuning_tournament.tex", sep="")
  unlink(TABLE)
  sink(TABLE, append=FALSE, split=TRUE)

  cat("\\begin{tabular}{@{\\extracolsep{\\fill}} cccc ccc}\\toprule\n", sep="")

  if (does_best_perform_sig_better == TRUE) {
    cat("Revision & Fixes & Authors & Time & Avg. & Better & Avg.\\\\\n", sep="")
    #cat("Weight & Weight & Weight & Range & Pos. & than & $d$\\\\\n", sep="")
    cat("Weight & Weight & Weight & Range & Pos. & than & $\\hat{A}$\\\\\n", sep="")
  } else {
    cat("Revision & Fixes  & Authors & Time  & Avg. &           & \\\\\n", sep="")
    cat("Weight   & Weight & Weight  & Range & Pos. & $\\sigma$ & CI\\\\\n", sep="")
  }

  cat("\\midrule\n", sep="")
  cat("\\rowcolor{gray!25}", "\n", sep="")
  cat("\\multicolumn{7}{c}{\\textit{top ", best_N, "}}", "\\\\", "\n", sep="")
  for (i in seq(1, best_N, by=1)) {
    elem <- tail(head(best_ranking, i), 1)
    cat(pretty_print_decimal_x(elem$"rw", 1), " & ", pretty_print_decimal_x(elem$"fw", 1),
                    " & ", pretty_print_decimal_x(elem$"aw", 1), " & ",  pretty_print_decimal_x(elem$"tr", 1),
                    " & ", pretty_print_decimal_x(elem$"mean_position", 2),
#                    " & ", pretty_print_decimal_x(elem$"mean_score", 2),
                    sep="")

    if (does_best_perform_sig_better == TRUE) {
      cat(" & ", elem$"points", " / ", num_confs,
                    " & ", pretty_print_decimal_x(elem$"mean_effect_size", 2),
                    "\\\\\n", sep="")
    } else {
      cat(" & ", pretty_print_decimal_x(elem$"configuration_positions_sd", 2),
          " & [", pretty_print_decimal_x(elem$"configuration_positions_ci_min", 2), ", ", pretty_print_decimal_x(elem$"configuration_positions_ci_max", 2), "]",
          "\\\\\n", sep="")
    }
  }
  cat("\\rowcolor{gray!25}", "\n", sep="")
  cat("\\multicolumn{7}{c}{\\textit{bottom ", worst_N, "}}", "\\\\", "\n", sep="")
  for (i in seq(1, worst_N, by=1)) {
    elem <- tail(head(worst_ranking, i), 1)
    cat(pretty_print_decimal_x(elem$"rw", 1), " & ", pretty_print_decimal_x(elem$"fw", 1),
                    " & ", pretty_print_decimal_x(elem$"aw", 1), " & ",  pretty_print_decimal_x(elem$"tr", 1),
                    " & ", pretty_print_decimal_x(elem$"mean_position", 2),
#                    " & ", pretty_print_decimal_x(elem$"mean_score", 2),
                    sep="")

    if (does_best_perform_sig_better == TRUE) {
      cat(" & ", elem$"points", " / ", num_confs,
                    " & ", pretty_print_decimal_x(elem$"mean_effect_size", 2),
                    "\\\\\n", sep="")
    } else {
      cat(" & ", pretty_print_decimal_x(elem$"configuration_positions_sd", 2),
          " & [", pretty_print_decimal_x(elem$"configuration_positions_ci_min", 2), ", ", pretty_print_decimal_x(elem$"configuration_positions_ci_max", 2), "]",
          "\\\\\n", sep="")
    }
  }

  cat("\\bottomrule\n", sep="")
  cat("\\end{tabular}\n", sep="")

  sink()

  BEST_BUG_PREDICTION_PARAMETERS = paste("../experiments/bug_prediction/bug_prediction_best_parameters.txt", sep="")
  unlink(BEST_BUG_PREDICTION_PARAMETERS)
  sink(BEST_BUG_PREDICTION_PARAMETERS, append=FALSE, split=TRUE)

  best <- head(best_ranking, 1)
  rw = pretty_print_decimal_x(best$"rw", 1)
  fw = pretty_print_decimal_x(best$"fw", 1)
  aw = pretty_print_decimal_x(best$"aw", 1)
  tr = pretty_print_decimal_x(best$"tr", 1)

  cat("REVISIONS_WEIGHT=", rw, "\n", sep="")
  cat("FIXES_WEIGHT=", fw, "\n", sep="")
  cat("AUTHORS_WEIGHT=", aw, "\n", sep="")
  cat("TIME_RANGE=", tr, "\n", sep="")

  sink()

  # if, for any reason, this line is changed, macros.R must be updated as well
  return(data.frame(stringsAsFactors=FALSE,
                    rw=rw, fw=fw, aw=aw, tr=tr,
                    position=pretty_print_decimal_x(best$"mean_position", 2),
                    score=pretty_print_decimal_x(best$"mean_score", 2),
                    better_than=best$"points",
                    effect_size=pretty_print_decimal_x(best$"mean_effect_size", 2),
                    num_confs=num_confs))
}

##
#
##
is_data_normally_distributed <- function() {

  dt <- load_TABLE(SIMPLE_DATA_FILE)

  comparisions <- hash()

  ##
  # Cache data so that we do not need to compute scores all the times
  data_cache <- hash()

  points <- c()
  for (revisions_weight in rws) {
    for (fixes_weight in fws) {
      for (authors_weight in aws) {
        if (is_valid_sum_of_weights(revisions_weight, fixes_weight, authors_weight) == FALSE) {
          next;
        }

        for (time_range in trs) {

          a_name = paste("rev-", revisions_weight, "_fix-", fixes_weight, "_aut-", authors_weight, "_tr-", time_range, sep="")
          if (has.key(a_name, data_cache) == FALSE) {
            data_cache[[a_name]] <- dt[which(dt$"rw" == revisions_weight &
                                             dt$"fw" == fixes_weight &
                                             dt$"aw" == authors_weight &
                                             dt$"tr" == time_range), ]
          }
          configuration_positions <- data_cache[[a_name]]$"position"
          configuration_scores <- data_cache[[a_name]]$"score"

          #points <- c(points, configuration_positions)
          for (rw in rws[rws != revisions_weight]) {
            for (fw in fws[fws != fixes_weight]) {
              for (aw in aws[aws != authors_weight]) {
                if (is_valid_sum_of_weights(rw, fw, aw) == FALSE) {
                  next;
                }

                for (tr in trs[trs != time_range]) {
                  stopifnot(revisions_weight != rw && fixes_weight != fw && authors_weight != aw && time_range != tr)

                  b_name = paste("rev-", rw, "_fix-", fw, "_aut-", aw, "_tr-", tr, sep="")
                  if (has.key(paste(a_name, "--vs--", b_name, sep=""), comparisions) == TRUE) {
                    cat(" - Skipping ", a_name, "--vs--", b_name, "\n", sep="")
                    next;
                  } else if (has.key(paste(b_name, "--vs--", a_name, sep=""), comparisions) == TRUE) {
                    cat(" + Skipping ", b_name, "--vs--", a_name, "\n", sep="")
                    next;
                  }
                  comparisions[[ paste(a_name, "--vs--", b_name, sep="") ]] <- 0

                  if (has.key(b_name, data_cache) == FALSE) {
                    data_cache[[b_name]] <- dt[which(dt$"rw" == rw &
                                                     dt$"fw" == fw &
                                                     dt$"aw" == aw &
                                                     dt$"tr" == tr), ]
                  }
                  positions <- data_cache[[b_name]]$"position"
                  scores <- data_cache[[b_name]]$"score"
                  stopifnot(length(positions) != 0)

                  #points <- c(points, configuration_positions - positions)
                  points <- c(points, positions)
                }
              }
            }
          }
        }
      }
    }
  }
  print(points)

  skewness <- pretty_print_decimal_x(skewness(points), 2) # ~0 if normal
  kurtosis <- pretty_print_decimal_x(kurtosis(points), 2) # ~3 if normal
  # Shapiro-Wilks test is affected by ties,
  shapiro_test <- NA #shapiro.test(points) # does not work with a very large sample size
  shapiro_value <- NA #pretty_print_decimal_x(shapiro_test$statistic, 2)
  shapiro_pv <- NA #sprintf("%.3g", shapiro_test$p.value)
  # but not as much as Anderson-Darling test
  ad_test <- ad.test(points)
  ad_test_value <- pretty_print_decimal_x(ad_test$statistic, 2)
  ad_test_pv <- sprintf("%.3g", ad_test$p.value)

  TABLE = paste(PAPER_DIRECTORY_PATH, "/tables/bug_prediction_tuning_distribution.tex", sep="")
  unlink(TABLE)
  sink(TABLE, append=FALSE, split=TRUE)

  cat("\\begin{tabular}{@{\\extracolsep{\\fill}} cc cc cc}\\toprule\n", sep="")
  cat("Skewness & Kurtosis & \\multicolumn{2}{c}{Shapiro-Wilk} & \\multicolumn{2}{c}{Anderson-Darling}\\\\\n", sep="")
  cat(" &  & \\textit{W} & \\textit{p-value} & \\textit{A} & \\textit{p-value}\\\\\n", sep="")
  cat("\\midrule\n", sep="")
  cat(skewness, " & ", kurtosis, " & ", shapiro_value, " & ",
      shapiro_pv, " & ", ad_test_value, " & ", ad_test_pv, "\\\\\n", sep="")
  cat("\\bottomrule\n", sep="")
  cat("\\end{tabular}\n", sep="")

  sink()

  pdfname <- paste(PAPER_DIRECTORY_PATH, "/figs/bug-prediction-tuning-hist.pdf", sep="")
  unlink(pdfname)
  pdf(file=pdfname, family="Helvetica", width=5, height=5)
    p <- hist(points)
    print(p)
  dev.off()
  embed_fonts(pdfname, options="-dSubsetFonts=true -dEmbedAllFonts=true -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dMaxSubsetPct=100")

  pdfname <- paste(PAPER_DIRECTORY_PATH, "/figs/bug-prediction-tuning-qqnorm.pdf", sep="")
  unlink(pdfname)
  pdf(file=pdfname, family="Helvetica", width=5, height=5)
    qqnorm(points)
    qqline(points, col=2)
  dev.off()
  embed_fonts(pdfname, options="-dSubsetFonts=true -dEmbedAllFonts=true -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dMaxSubsetPct=100")

  # if, for any reason, this line is changed, macros.R must be updated as well
  #return(c(skewness, kurtosis, shapiro_value, shapiro_pv, ad_test_value, ad_test_pv))
}

##
#
##
print_ranking_score <- function(revisions_weight, fixes_weight, authors_weight, time_range) {

  dt <- load_TABLE(SIMPLE_DATA_FILE)
  dt <- dt[which(dt$"rw" == revisions_weight &
                 dt$"fw" == fixes_weight &
                 dt$"aw" == authors_weight &
                 dt$"tr" == time_range), ]

  for (pid in unique(dt$"pid")) {
    for (bid in unique(dt$"bid"[dt$"pid" == pid])) {
      cat(pid, "-", bid, " & ", dt$"position"[dt$"pid" == pid & dt$"bid" == bid], "\n", sep="")
    }
  }
}

##
#
##
compare_two_configurations <- function(rw_a, fw_a, aw_a, tr_a, rw_b, fw_b, aw_b, tr_b) {

  dt <- load_TABLE(SIMPLE_DATA_FILE)

  position_a <- dt$"position"[which(dt$"rw" == rw_a &
                                     dt$"fw" == fw_a &
                                     dt$"aw" == aw_a &
                                     dt$"tr" == tr_a)]
  position_b <- dt$"position"[which(dt$"rw" == rw_b &
                                     dt$"fw" == fw_b &
                                     dt$"aw" == aw_b &
                                     dt$"tr" == tr_b)]

  cat("Positions a: ", paste(position_a, collapse=":"), "\n", sep="")
  cat("Mean(position) a: ", mean(position_a), "\n", sep="")
  cat("Positions b: ", paste(position_b, collapse=":"), "\n", sep="")
  cat("Mean(position) b: ", mean(position_b), "\n", sep="")

  effect_size <- A12(position_a, position_b)

  # was it significant?
  if (wilcox_test(position_a, position_b) == TRUE) {
    cat("Significant effect-size ", effect_size, "\n", sep="")
  } else {
    cat("Not significant effect-size ", effect_size, "\n", sep="")
  }
}

# EOF

