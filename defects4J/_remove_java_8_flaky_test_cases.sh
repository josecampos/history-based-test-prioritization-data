#!/bin/bash

#SCRIPT_DIR=$(cd `dirname $0` && pwd)
SCRIPT_DIR=`pwd`
source "$SCRIPT_DIR/../experiments/utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether JAVA_HOME is set
[ "$JAVA_HOME" != "" ] || die "[ERROR] JAVA_HOME is not set!"
# Check whether ANT_HOME is set
[ "$ANT_HOME" != "" ] || die "[ERROR] ANT_HOME is not set!"
# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "[ERROR] D4J_HOME is not set!"

USAGE="Usage: $0 <pid> <bid>"
[ $# -eq 2 ] || die "$USAGE";
PID="$1"
BID="$2"

export PATH="$JAVA_HOME/bin:$ANT_HOME/bin:$PATH"

TRIGGERING_TEST_CASES_FILE="$D4J_HOME/framework/projects/$PID/trigger_tests/$BID"
FLAKY_TEST_CASES_FILE="$D4J_HOME/framework/projects/$PID/dependent_tests"

# --------------------------------------------------------------- Main

echo "PID: $$"
hostname

echo ""
echo "[INFO] Checkout $PID-${BID}b"
tmp_dir=$(_checkout "$PID" "$BID" "b");
if [ $? -ne 0 ]; then
  echo "[ERROR] Checkout of the BUGGY version has failed!"
  rm -rf "$tmp_dir"
  exit 1;
fi

echo ""
echo "[INFO] Compile $PID-${BID}b"
_compile "$tmp_dir"
if [ $? -ne 0 ]; then
  echo "[ERROR] Compilation of $PID-${BID}b has failed!"
  rm -rf "$tmp_dir"
  exit 1;
fi

# -------------------------------- Run all manually-written test cases

echo ""
echo "[INFO] Running all manually-written test cases on $PID-${BID}b"
"$D4J_HOME/framework/bin/defects4j" test -w "$tmp_dir"
if [ $? -ne 0 ]; then
  echo "[ERROR] Execution of all manually-written test cases of $PID-${BID}b has failed!"
  rm -rf "$tmp_dir"
  exit 1;
fi

local_triggering_test_cases="$tmp_dir/failing_tests"
if [ ! -s "$local_triggering_test_cases" ]; then
  echo "[ERROR] '$local_triggering_test_cases' is empty or does not exist!"
  rm -rf "$tmp_dir"
  exit 1;
fi

while read -r triggering_test_case; do
  if grep -q "^$triggering_test_case$" "$TRIGGERING_TEST_CASES_FILE"; then
    echo "[INFO] Test case '$triggering_test_case' is a triggering test case."
    continue;
  fi

  if ! grep -q "^$triggering_test_case$" "$FLAKY_TEST_CASES_FILE"; then
    echo "[INFO] Test case '$triggering_test_case' seems to be a flaky test case (maybe due to Java-8). Adding it to the list of flaky/dependent test cases."
    echo "$triggering_test_case" >> "$FLAKY_TEST_CASES_FILE"
  fi
done < <(grep "^--- " "$local_triggering_test_cases")

# ----------------------------------------------------------- Clean up

echo ""
echo "[INFO] Clean up"

rm -rf "$tmp_dir"

echo "DONE!"
exit 0;

# EOF
