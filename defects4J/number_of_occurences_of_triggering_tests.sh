  #!/bin/bash
#
# --------------------------------------------------------------------
# This script extracts the number of occurences of all triggering test
# cases from the test-evolution-data.
#
# Usage:
# number_of_occurences_of_triggering_tests.sh <data dir> <output file>
#
# Requirements:
#   Execution of tools/get_tools.sh script.
#   Execution of experiments/run_test_evolution_analysis.sh script.
# --------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../experiments/utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether D4J_HOME exists
D4J_HOME="$SCRIPT_DIR/../tools/defects4j"
if [ ! -d "$D4J_HOME" ]; then
  die "Could not find 'Defects4J' home directory! Did you run 'tools/get_tools.sh'?"
else
  export D4J_HOME="$D4J_HOME"
fi

USAGE="Usage: $0 <data dir> <output file>"
[ $# -eq 2 ] || die "$USAGE";

DATA_DIR=$1
if [ ! -d "$DATA_DIR" ]; then
  die "'$DATA_DIR' does not exist"
fi

OUTPUT_FILE=$2
echo "pid,bid,triggering_test_case,num_occurences,num_failures" > "$OUTPUT_FILE"

TOOL="manual" # TODO adapt script in case of automatically generated test cases

# --------------------------------------------------------------- Main

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"

  # Get all bugs
  bids=$(cut -f1 -d',' $dir_project/commit-db)

  # Iterate over all bugs
  for bid in $bids; do
    input_root_dir="$DATA_DIR/$pid/$bid"
    if [ ! -d "$input_root_dir" ]; then
      echo "[WARNING] '$input_root_dir' does not exist"
      continue
    fi

    echo "$pid-$bid"

    log_file_tmp="/tmp/log_file_tmp_$USER-$pid-$bid-$TOOL-$$.txt"
    >"$log_file_tmp"

    ##
    # have we got test evolution data?
    test_evolution_data_zip="$input_root_dir/$TOOL/$pid-$bid-$TOOL-test-evolution-data.tar.bz2"
    if [ -s "$test_evolution_data_zip" ]; then
      tar -xf "$test_evolution_data_zip" test-evolution-data/log.txt -O > "$log_file_tmp"
      if [ $? -ne 0 ]; then
        rm "$log_file_tmp"
        echo "[ERROR] It was not possible to extract 'log.txt' from '$test_evolution_data_zip', therefore a sanity-check on $pid-$bid could not be performed."
        continue;
      fi

      # skip pid-bid if it has not completed successfully
      if ! grep -q "^DONE\!$" "$log_file_tmp" || grep -q " No space left on device$" "$log_file_tmp"; then
        rm "$log_file_tmp"
        echo "[ERROR] Test-evolution analysis of $pid-$bid has failed!"
        continue;
      fi

      rm "$log_file_tmp"
    else
      echo "[ERROR] No test-evolution data for $pid-$bid!"
      continue;
    fi

    data_file_tmp="/tmp/data_file_tmp_$USER-$pid-$bid-$TOOL-$$.csv"
    >"$data_file_tmp"

    tar -xf "$test_evolution_data_zip" test-evolution-data/test_evolution.csv -O > "$data_file_tmp"
    if [ $? -ne 0 ]; then
      rm "$data_file_tmp"
      echo "[ERROR] It was not possible to extract 'test_evolution.csv' from '$test_evolution_data_zip', therefore analysis cannot be performed."
      continue;
    fi

    for triggering_test_case in $(grep -a "^--- " "$D4J_HOME/framework/projects/$pid/trigger_tests/$bid" | cut -f2 -d' '); do
      num_occurences=$(grep "^$pid,${bid}b,[0-9]*,-[0-9]*,$triggering_test_case," "$data_file_tmp" | wc -l)
      num_failures=$(grep "^$pid,${bid}b,[0-9]*,-[0-9]*,$triggering_test_case,[0-9]*,fail," "$data_file_tmp" | wc -l)

      # sanity check: number of failures can't be higher than number of occurences
      if [ "$num_failures" -gt "$num_occurences" ]; then
        die "[ERROR] Number of failures of triggering test case '$triggering_test_case' is higher than the number of occurences ($num_failures > $num_occurences)!"
      fi

      echo "$pid,$bid,$triggering_test_case,$num_occurences,$num_failures" >> "$OUTPUT_FILE"
    done

    rm "$data_file_tmp"
  done
done

## report projects/bugs with at least one triggering test case(s) failing in past revisions
OCCURENCES_GREATE_THAN_ONE_OUTPUT_FILE=$(echo "$(dirname $OUTPUT_FILE)/$(basename $OUTPUT_FILE .csv)-greater-than-one.csv")
echo "pid,bid" > "$OCCURENCES_GREATE_THAN_ONE_OUTPUT_FILE"

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"
  for bid in $(cut -f1 -d',' $dir_project/commit-db); do
    if ! grep -q "^$pid,$bid," "$OUTPUT_FILE"; then
      continue
    fi

    if ! grep -q "^$pid,$bid,.*,0,[0-9]*$" "$OUTPUT_FILE"; then
      echo "$pid,$bid" >> "$OCCURENCES_GREATE_THAN_ONE_OUTPUT_FILE"
    fi
  done
done

echo "DONE!"

# EOF

