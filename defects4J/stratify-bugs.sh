#!/bin/bash
#
# --------------------------------------------------------------------
# This script constructs a stratified random sample of 5 bugs per D4J
# project.
#
# Usage:
# stratify-bugs.sh
#
# Requirements:
#   Execution of tools/get_tools.sh script.
# --------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../experiments/utils.sh" || exit 1

# -------------------------------------------------------- Envs & Vars

# Check whether D4J_HOME exists
D4J_HOME="$SCRIPT_DIR/../tools/defects4j"
if [ ! -d "$D4J_HOME" ]; then
  die "Could not find 'Defects4J' home directory! Did you run 'tools/get_tools.sh'?"
else
  export D4J_HOME="$D4J_HOME"
fi

BUGS_FILE_STRATIFIED="$SCRIPT_DIR/bugs.stratified.txt"
echo "project_name,bug_id" > "$BUGS_FILE_STRATIFIED"

MAX_NUMBER_BUGS=5

# --------------------------------------------------------------- Main

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"
  num_bugs=$(wc -l "$dir_project/commit-db" | cut -f1 -d' ')

  i=0
  while :; do

    bid=$(shuf -i 1-$num_bugs -n 1)
    if grep -q "^$pid,$bid$" "$BUGS_FILE_STRATIFIED"; then
      continue;
    fi

    i=$((i+1))
    echo "$pid,$bid" >> "$BUGS_FILE_STRATIFIED"

    if [ "$i" -eq "$MAX_NUMBER_BUGS" ]; then
      break;
    fi
  done
done

## sort stratified file

(head -n 1 "$BUGS_FILE_STRATIFIED" && tail -n +2 "$BUGS_FILE_STRATIFIED" | sort -t , -k1,1 -k2,2n) > "$BUGS_FILE_STRATIFIED.sorted"
mv -f "$BUGS_FILE_STRATIFIED.sorted" "$BUGS_FILE_STRATIFIED"

# EOF

