#!/bin/bash
#
# --------------------------------------------------------------------
# This script submits as many jobs as the number of projects * number
# of bugs. Each job runs all manually-written test cases to identify
# and discard those that fail on Java 8 but are not related to the
# bug.
#
# Usage:
# remove_java_8_flaky_test_cases.sh
#
# Parameters:
#   <DATA DIR>   Directory to which the log files will be written. The
#                script will create the following directory structure:
#                   DATA DIR/project/bug_id/
#
# Requirements:
#   Execution of tools/get_tools.sh script.
# --------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../experiments/utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether ANT_HOME exists
ANT_HOME="$SCRIPT_DIR/../tools/apache-ant"
if [ ! -d "$ANT_HOME" ]; then
  die "Could not find java 'apache-ant' directory! Did you run 'tools/get_tools.sh'?"
else
  export ANT_HOME="$ANT_HOME"
fi

# Check whether D4J_HOME exists
D4J_HOME="$SCRIPT_DIR/../tools/defects4j"
if [ ! -d "$D4J_HOME" ]; then
  die "Could not find 'Defects4J' home directory! Did you run 'tools/get_tools.sh'?"
else
  export D4J_HOME="$D4J_HOME"
fi

# Check whether JAVA_HOME exists
JAVA_HOME="$SCRIPT_DIR/../tools/jdk-8"
if [ ! -d "$JAVA_HOME" ]; then
  die "Could not find java '$JAVA_HOME' directory! Did you run 'tools/get_tools.sh'?"
else
  export JAVA_HOME="$JAVA_HOME"
fi

USAGE="Usage: $0"
[ $# -eq 1 ] || die "$USAGE";
DATA_DIR=$1

rm -rf "$DATA_DIR"; mkdir -p "$DATA_DIR"

# --------------------------------------------------------------- Main

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"

  # Get all bugs
  bids=$(cut -f1 -d',' $dir_project/commit-db)

  # Iterate over all bugs
  for bid in $bids; do
    bid_data_dir="$DATA_DIR/$pid/$bid"
    mkdir -p "$bid_data_dir"

    log_file="$bid_data_dir/log.txt"

    echo "$pid-$bid"
    echo "$pid-$bid" > "$log_file"

    pushd . > /dev/null 2>&1
    cd "$SCRIPT_DIR"
      if _am_I_a_cluster; then
        qsub -V -N "_$bid-$pid-remove-java-8-flaky-test-cases" -l h_rt=04:00:00 -l rmem=8G -e "$log_file" -o "$log_file" -j y "_remove_java_8_flaky_test_cases.sh" "$pid" "$bid";
      else
        timeout --signal=KILL 4h bash "_remove_java_8_flaky_test_cases.sh" "$pid" "$bid" > "$log_file" 2>&1 &
        _register_background_process $!;
        _can_more_jobs_be_submitted;
      fi
    popd > /dev/null 2>&1
  done
done

# It might be the case that there are still jobs running
_wait_for_jobs;

echo "All jobs submitted!"

# EOF

