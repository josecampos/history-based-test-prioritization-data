#!/bin/bash

#SCRIPT_DIR=$(cd `dirname $0` && pwd)
SCRIPT_DIR=`pwd`
source "$SCRIPT_DIR/../experiments/utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether JAVA_HOME is set
[ "$JAVA_HOME" != "" ] || die "[ERROR] JAVA_HOME is not set!"
# Check whether ANT_HOME is set
[ "$ANT_HOME" != "" ] || die "[ERROR] ANT_HOME is not set!"
# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "[ERROR] D4J_HOME is not set!"
# Check whether KANONIZO_JAR is set
[ "$KANONIZO_JAR" != "" ] || die "[ERROR] KANONIZO_JAR is not set!"

USAGE="Usage: $0 <pid> <bid> <data dir>"
[ $# -eq 3 ] || die "$USAGE";
PID="$1"
BID="$2"
DATA_DIR="$3"

export PATH="$JAVA_HOME/bin:$ANT_HOME/bin:$PATH"

LOCAL_TMP_DIR="/tmp/$USER/_kanonizo_sanity_check-$PID-$BID-$$"
rm -rf "$LOCAL_TMP_DIR"; mkdir -p "$LOCAL_TMP_DIR"

KANONIZO_LOGS_DIR="$LOCAL_TMP_DIR/.kanonizo-logs"
KANONIZO_ORDERING_FILE="$KANONIZO_LOGS_DIR/ordering/$PID-${BID}b.csv"

# --------------------------------------------------------------- Main

echo "PID: $$"
hostname

echo ""
echo "[INFO] Checkout $PID-${BID}b"
tmp_dir=$(_checkout "$PID" "$BID" "b");
if [ $? -ne 0 ]; then
  echo "[ERROR] Checkout of the BUGGY version has failed!"
  rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
  exit 1;
fi

echo ""
echo "[INFO] Compile $PID-${BID}b"
_compile "$tmp_dir"
if [ $? -ne 0 ]; then
  echo "[ERROR] Compilation of $PID-${BID}b has failed!"
  rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
  exit 1;
fi

CP=$(_get_classpath "$tmp_dir")
if [ $? -ne 0 ]; then
  echo "[ERROR] Get classpath of $PID-${BID}b has failed!"
  rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
  exit 1;
fi

# ------------------------------------------------------- Run Kanonizo

pushd . > /dev/null 2>&1
cd "$tmp_dir"

  dir_bin_classes=$(_get_classes_target_directory "$tmp_dir")
  echo "[DEBUG] dir_bin_classes: $dir_bin_classes"
  dir_bin_test_classes=$(_get_test_classes_target_directory "$tmp_dir")
  echo "[DEBUG] dir_bin_test_classes: $dir_bin_test_classes"

  echo ""
  echo "[INFO] Running Kanonizo on $PID-${BID}b"
  java -cp "$CP:$KANONIZO_JAR" -javaagent:"$KANONIZO_JAR" \
     org.kanonizo.Main \
    -s "$dir_bin_classes" \
    -t "$dir_bin_test_classes" \
    -Dprioritise=true \
    -Dscythe_write=true \
    -Dlog_dir="$KANONIZO_LOGS_DIR" \
    -Dlog_filename="$PID-${BID}b" \
    -Dscythe_filename="$PID-${BID}b-coverage.json" \
    -Duse_timeout=false \
    -Duse_suite_methods=true \
    -a greedy
  if [ $? -ne 0 ]; then
    echo "[ERROR] Execution of Kanonizo on $PID-${BID}b has failed!"
    popd > /dev/null 2>&1
    rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
    exit 1;
  fi

  if [ ! -s "$KANONIZO_ORDERING_FILE" ]; then
    echo "[ERROR] '$KANONIZO_ORDERING_FILE' does not exit or it is empty!"
    popd > /dev/null 2>&1
    rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
    exit 1;
  fi

popd > /dev/null 2>&1

# ------------------------------------------------------- Sanity check

## 0. Does Kanonizo report duplicate test cases?

num_test_cases_kanonizo=$(tail -n +2 "$KANONIZO_ORDERING_FILE" | wc -l)
num_unique_test_cases_kanonizo=$(tail -n +2 "$KANONIZO_ORDERING_FILE" | cut -f1 -d',' | sort -u | wc -l)

if [ "$num_test_cases_kanonizo" -ne "$num_unique_test_cases_kanonizo" ]; then
  echo "[ERROR] Kanonizo is reporting duplicate test cases! Total number of test cases reported is '$num_test_cases_kanonizo' and the number of unique test cases is '$num_unique_test_cases_kanonizo'"

  cp -Rv $LOCAL_TMP_DIR/.kanonizo-logs/* "$DATA_DIR/" # debug

  rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
  exit 1;
fi

## 1. Does Kanonizo and D4J agree on the number of triggering test cases?

num_triggering_test_cases_kanonizo=$(grep ",false," "$KANONIZO_ORDERING_FILE" | wc -l)
num_triggering_test_cases_d4j=$(grep "^--- " "$D4J_HOME/framework/projects/$PID/trigger_tests/$BID" | wc -l)

if [ "$num_triggering_test_cases_kanonizo" -ne "$num_triggering_test_cases_d4j" ]; then
  echo "[ERROR] Number of triggering test cases reported by Kanonizo ($num_triggering_test_cases_kanonizo) is not the same as reported by D4J ($num_triggering_test_cases_d4j)!"

  cp -Rv $LOCAL_TMP_DIR/.kanonizo-logs/* "$DATA_DIR/" # debug

  rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
  exit 1;
fi

## 2. Does Kanonizo and D4J agree on the list of triggering test cases?

agree=0
grep "^--- " "$D4J_HOME/framework/projects/$PID/trigger_tests/$BID" | while read -r trigger_test; do
  class_test_name=$(echo "$trigger_test" | cut -f2 -d' ' | cut -f1 -d':')
  unit_test_name=$(echo "$trigger_test" | cut -f2 -d' ' | cut -f3 -d':')

  # e.g., constructor_is_called_for_each_test_in_test_class(org.mockitousage.annotation.MockInjectionUsingConstructorTest),19,false,org.junit.ComparisonFailure: ...
  if ! grep -q "^$unit_test_name($class_test_name),[0-9]*,false," "$KANONIZO_ORDERING_FILE"; then
    echo "[ERROR] Triggering test case '$class_test_name::$unit_test_name' has not been reported by Kanonizo!"
    agree=1
  fi
done

if [ "$agree" -eq "1" ]; then
  cp -Rv $LOCAL_TMP_DIR/.kanonizo-logs/* "$DATA_DIR/" # debug

  rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
  exit 1;
fi

# -------------------------------------------- Collect data & Clean up

echo ""
echo "[INFO] Collect data & Clean up"

pushd . > /dev/null 2>&1
cd "$LOCAL_TMP_DIR"

  # zip everything
  tar -jcvf "$PID-$BID-kanonizo-sanity-check.tar.bz2" ".kanonizo-logs"
  if [ $? -ne 0 ]; then
    echo "[ERROR] It was not possible to compress directory '$LOCAL_TMP_DIR/.kanonizo-logs/'!"

    echo "[INFO] Copying all files from local '$LOCAL_TMP_DIR/.kanonizo-logs' to remote '$DATA_DIR/' so that anyone can debug them"
    cp -Rv .kanonizo-logs/* "$DATA_DIR/"

    popd > /dev/null 2>&1
    rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
    exit 1;
  else
    cp -v "$PID-$BID-kanonizo-sanity-check.tar.bz2" "$DATA_DIR/"
  fi

  echo "DONE!"
popd > /dev/null 2>&1

# do not leave anything behind
rm -rf "$tmp_dir" "$LOCAL_TMP_DIR" > /dev/null 2>&1

exit 0;

# EOF

