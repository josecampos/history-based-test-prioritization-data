Directory with all data produced by our experiments' scripts
============================================================

Structure of each project
-------------------------

  project-name/
    bug-id/
      <project-name>-<bug-id>-bug-prediction-data.tar.bz2
      manual/
        <project-name>-<bug-id>-manual-coverage-analysis.tar.bz2
    ...
  ...
