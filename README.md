# History-based Test Case Prioritization Data Repository

This repository contains data files, data-collection scripts, and data-analysis
scripts of the
[An Empirical Study on the Use of Defect Prediction for Test Case Prioritization](https://jose.github.io/assets/pdfs/ICST2019-prioritization-paper.pdf)
project.

## Contents

* `analysis/`: R scripts that crunch the data to produce numbers for the paper.
* `data/`: Data files for the final results and corresponding support scripts.
* `experiments/`: Scripts for running the experiments described in the paper.
* `tools/`: External tools required to run the experiments described in the paper, e.g., Java, Kanonizo, etc.
