#!/bin/bash

#SCRIPT_DIR=$(cd `dirname $0` && pwd)
SCRIPT_DIR=`pwd`
source "$SCRIPT_DIR/../experiments/utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether JAVA_7_HOME is set
[ "$JAVA_7_HOME" != "" ] || die "[ERROR] JAVA_7_HOME is not set!"
# Check whether JAVA_8_HOME is set
[ "$JAVA_8_HOME" != "" ] || die "[ERROR] JAVA_8_HOME is not set!"
# Check whether ANT_HOME is set
[ "$ANT_HOME" != "" ] || die "[ERROR] ANT_HOME is not set!"
# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "[ERROR] D4J_HOME is not set!"
# Check whether MINION_EXE is set
[ "$MINION_EXE" != "" ] || die "[ERROR] MINION_EXE is not set!"
# Check whether KANONIZO_JAR is set
[ "$KANONIZO_JAR" != "" ] || die "[ERROR] KANONIZO_JAR is not set!"

USAGE="Usage: $0 <pid> <bid> <tool: manual, evosuite, or randoop> <data dir> <coverage-data zip file> <bug-prediction data zip file> <test-evolution data zip file> <prioritisation algorithm> <secondary objective (only for schwa algorithm)> <classes per group> <bug_prediction_type: default|best|n_commits> <number of commits, in case bug_prediction_type=n_commits"
[ $# -eq 12 ] || die "$USAGE";
PID="$1"
BID="$2"
TOOL="$3"
DATA_DIR="$4"
COVERAGE_DATA_ZIP="$5"
BUG_PREDICTION_DATA_ZIP="$6"
TEST_EVOLUTION_DATA_ZIP="$7"
ALGORITHM="$8"
SECONDARY_OBJECTIVE="$9"
CLASSES_PER_GROUP="${10}"
BUG_PREDICTION_TYPE="${11}"
BUG_PREDICTION_NUM_COMMITS="${12}"

MAX_NUM_SEEDS="30"

if [ "$BUG_PREDICTION_TYPE" == "n_commits" ]; then
  if [ "$BUG_PREDICTION_NUM_COMMITS" -eq "-1" ]; then
    die "Bug prediction type '$BUG_PREDICTION_TYPE' cannot have a negative number of commits"
  fi
fi

export PATH="$ANT_HOME/bin:$PATH"
PATH_BACKUP="$PATH"

LOCAL_TMP_DIR="/tmp/$USER/_run_prioritisation_techniques-$PID-$BID-$$"
rm -rf "$LOCAL_TMP_DIR"; mkdir -p "$LOCAL_TMP_DIR"

LOCAL_DATA_DIR="$LOCAL_TMP_DIR/prioritisation-data"
mkdir -p "$LOCAL_DATA_DIR"

# --------------------------------------------------------------- util

##
#
##
_prioritize() {
  local USAGE="Usage: _prioritize <checkout dir> <classpath> <dir_bin_classes> <dir_bin_test_classes> <algorithm_data_dir> <algorithm> <secondary_objective> <% of classes> <random seed> <similarity coverage adequacy>"
  if [ $# -ne 10 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local tmp_dir="$1"
  local CP="$2"
  local dir_bin_classes="$3"
  local dir_bin_test_classes="$4"
  local algorithm_data_dir="$5"
  local algorithm="$6"
  local secondary_objective="$7"
  local classes_per_group="$8"
  local random_seed="$9"
  local similarity_coverage_adequacy="${10}"
  local use_percentage_classes=true

  if [ "$classes_per_group" -eq "0" ]; then
    classes_per_group="1"
    use_percentage_classes=false
  fi

  java -Djava.io.tmpdir="$LOCAL_TMP_DIR" -cp "$CP:$KANONIZO_JAR" -javaagent:"$KANONIZO_JAR" \
       org.kanonizo.Main \
       -Dprogressbar_enable=false \
      -r "$tmp_dir" \
      -s "$dir_bin_classes" \
      -t "$dir_bin_test_classes" \
      -Drandom_seed="$random_seed" \
      -Dprioritise=true \
      -Dinstrumenter=gzoltar \
      -Dlog_dir="$algorithm_data_dir" \
      -Dlog_filename="$PID-${BID}b-$algorithm" \
      -Dgz_file="$LOCAL_TMP_DIR/coverage-data/gzoltar.ser" \
      -Dschwa_file="$LOCAL_TMP_DIR/bug-prediction-data/$PID-${BID}b.json" \
      -Dschwa_secondary_objective="$secondary_objective" \
      -Dclasses_per_group="$classes_per_group" \
      -Duse_percentage_classes=$use_percentage_classes \
      -Dconstraint_solver_path="$MINION_EXE" \
      -Dhistory_file="$LOCAL_TMP_DIR/test-evolution-data/test_evolution.csv" \
      -Dsimilarity_coverage_adequacy=$similarity_coverage_adequacy \
      -a "$algorithm"

  if [ $? -ne 0 ]; then
    echo "[ERROR] Execution of Kanonizo on $PID-${BID}b --- $algorithm algorithm has failed!"
    return 1;
  fi

  local kanonizo_ordering_file="$algorithm_data_dir/ordering/$PID-${BID}b-$algorithm.csv"
  if [ ! -s "$kanonizo_ordering_file" ]; then
    echo "[ERROR] '$kanonizo_ordering_file' does not exit or it is empty!"
    return 1;
  fi

  local total_num_triggering_test_cases=$(grep -n ",[0-9]*,false," "$kanonizo_ordering_file" | wc -l | cut -f1 -d' ')
  if [ "$total_num_triggering_test_cases" -eq "0" ]; then
    echo "[ERROR] The ordering file created by '$algorithm' does not include any triggering test case!"
    return 1;
  fi

  ##
  # Perform a simple test case prioritisation analysis, e.g., what
  # is the position of the first triggering test case

  local total_num_test_cases=$(wc -l "$kanonizo_ordering_file" | cut -f1 -d' ')
  total_num_test_cases=$((total_num_test_cases-1)) # because the ordering file includes a header

  all_ranking_positions=()
  # test2947660(org.jfree.chart.renderer.category.junit.AbstractCategoryItemRendererTests),3,false,junit.framework.AssertionFailedError: ...,459
  for test_ranking_position in `grep -n ",[0-9]*,false," "$kanonizo_ordering_file" | cut -f1 -d':'`; do
    test_ranking_position=$((test_ranking_position-1))
    all_ranking_positions+=("$test_ranking_position")
  done

  local min=$(_min "$all_ranking_positions")
  local max=$(_max "$all_ranking_positions")
  local mean=$(_mean "$all_ranking_positions")
  local median=$(_median "$all_ranking_positions")

  if [ "$secondary_objective" == "similarity" ]; then
    echo "$PID,$BID,$algorithm,$secondary_objective-$similarity_coverage_adequacy,$classes_per_group,$random_seed,$min,$max,$mean,$median,$total_num_test_cases" >> "$TEST_CASE_PRIORITISATION_FILE"
  else
    echo "$PID,$BID,$algorithm,$secondary_objective,$classes_per_group,$random_seed,$min,$max,$mean,$median,$total_num_test_cases" >> "$TEST_CASE_PRIORITISATION_FILE"
  fi

  return 0
}

# --------------------------------------------------------------- Main

export PATH="$JAVA_7_HOME/bin:$PATH_BACKUP" # compile with Java-7

echo "PID: $$"
hostname

echo ""
echo "[INFO] Checkout $PID-${BID}b"
tmp_dir=$(_checkout "$PID" "$BID" "b");
if [ $? -ne 0 ]; then
  echo "[ERROR] Checkout of the BUGGY version has failed!"
  rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
  exit 1;
fi

echo ""
echo "[INFO] Compile $PID-${BID}b"
_compile "$tmp_dir"
if [ $? -ne 0 ]; then
  echo "[ERROR] Compilation of $PID-${BID}b has failed!"
  rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
  exit 1;
fi

CP=$(_get_classpath "$tmp_dir")
if [ $? -ne 0 ]; then
  echo "[ERROR] Get classpath of $PID-${BID}b has failed!"
  rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
  exit 1;
fi

# -------------------------------------------------- Get required data

pushd . > /dev/null 2>&1
cd "$LOCAL_TMP_DIR"

  echo ""
  echo "[INFO] Getting data (e.g., coverage) to run any prioritisation algorithm"

  ##
  # Get coverage data

  tar -jxvf "$COVERAGE_DATA_ZIP"
  if [ $? -ne 0 ]; then
    echo "[ERROR] Extracting '$COVERAGE_DATA_ZIP' to '$LOCAL_TMP_DIR' has failed!"
    popd > /dev/null 2>&1
    rm -rf "$LOCAL_TMP_DIR"
    exit 1;
  fi

  if [ ! -d "coverage-data" ]; then
    echo "[ERROR] There is not any 'coverage-data' directory under '$LOCAL_TMP_DIR'!"
    popd > /dev/null 2>&1
    rm -rf "$LOCAL_TMP_DIR"
    exit 1;
  fi

  if [ ! -s "coverage-data/gzoltar.ser" ]; then
    echo "[ERROR] There is not any 'coverage-data/gzoltar.ser' file!"
    popd > /dev/null 2>&1
    rm -rf "$LOCAL_TMP_DIR"
    exit 1;
  fi

  ##
  # Get bug-prediction data
  if [ "$ALGORITHM" == "schwa" ]; then
    tar -jxvf "$BUG_PREDICTION_DATA_ZIP"
    if [ $? -ne 0 ]; then
      echo "[ERROR] Extracting '$BUG_PREDICTION_DATA_ZIP' to '$LOCAL_TMP_DIR' has failed!"
      popd > /dev/null 2>&1
      rm -rf "$LOCAL_TMP_DIR"
      exit 1;
    fi

    if [ ! -d "bug-prediction-data" ]; then
      echo "[ERROR] There is not any 'bug-prediction-data' directory under '$LOCAL_TMP_DIR'!"
      popd > /dev/null 2>&1
      rm -rf "$LOCAL_TMP_DIR"
      exit 1;
    fi
  fi

  ##
  # Get test-evolution data
  if [ "$ALGORITHM" == "marijan" ] || [ "$ALGORITHM" == "elbaum" ] || [ "$ALGORITHM" == "huang" ] || [ "$ALGORITHM" == "cho" ]; then
    tar -jxvf "$TEST_EVOLUTION_DATA_ZIP"
    if [ $? -ne 0 ]; then
      echo "[ERROR] Extracting '$TEST_EVOLUTION_DATA_ZIP' to '$LOCAL_TMP_DIR' has failed!"
      popd > /dev/null 2>&1
      rm -rf "$LOCAL_TMP_DIR"
      exit 1;
    fi

    if [ ! -d "test-evolution-data" ]; then
      echo "[ERROR] There is not any 'test-evolution-data' directory under '$LOCAL_TMP_DIR'!"
      popd > /dev/null 2>&1
      rm -rf "$LOCAL_TMP_DIR"
      exit 1;
    fi
  fi
popd > /dev/null 2>&1

# ----------------------------------------------------- Prioritisation

export PATH="$JAVA_8_HOME/bin:$PATH_BACKUP" # run Kanonizo with Java-8

TEST_CASE_PRIORITISATION_FILE="$LOCAL_DATA_DIR/test_case_prioritisation.csv"
echo "project,bug_id,algorithm,secondary_objective,classes_per_group,random_seed,first,last,mean,median,num_test_cases" > "$TEST_CASE_PRIORITISATION_FILE"

pushd . > /dev/null 2>&1
cd "$tmp_dir"

  dir_bin_classes=$(_get_classes_target_directory "$tmp_dir")
  echo "[DEBUG] dir_bin_classes: $dir_bin_classes"
  dir_bin_test_classes=$(_get_test_classes_target_directory "$tmp_dir")
  echo "[DEBUG] dir_bin_test_classes: $dir_bin_test_classes"

  echo ""
  echo "[INFO] Running Kanonizo on $PID-${BID}b --- $ALGORITHM algorithm --- $SECONDARY_OBJECTIVE secondary objective"

  if [ "$ALGORITHM" == "schwa" ]; then
    for random_seed in $(seq 1 $MAX_NUM_SEEDS); do
      for classes_per_group in $(echo $CLASSES_PER_GROUP | tr ':' '\n'); do
        for similarity_coverage_adequacy in 50 100; do
          algorithm_data_dir="$LOCAL_DATA_DIR/$ALGORITHM-$SECONDARY_OBJECTIVE-$classes_per_group-$random_seed-$similarity_coverage_adequacy"
          mkdir -p "$algorithm_data_dir"

          _prioritize "$tmp_dir" "$CP" "$dir_bin_classes" "$dir_bin_test_classes" "$algorithm_data_dir" "$ALGORITHM" "$SECONDARY_OBJECTIVE" "$classes_per_group" "$random_seed" "$similarity_coverage_adequacy"
          if [ $? -ne 0 ]; then
            popd > /dev/null 2>&1
            rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
            exit 1;
          fi

          if [ "$SECONDARY_OBJECTIVE" != "similarity" ]; then
            break
          fi
        done
      done

      if [ "$SECONDARY_OBJECTIVE" != "random" ]; then
        break
      fi
    done
  else
    for random_seed in $(seq 1 $MAX_NUM_SEEDS); do
      algorithm_data_dir="$LOCAL_DATA_DIR/$ALGORITHM-$random_seed"
      mkdir -p "$algorithm_data_dir"

      _prioritize "$tmp_dir" "$CP" "$dir_bin_classes" "$dir_bin_test_classes" "$algorithm_data_dir" "$ALGORITHM" "none" "0" "$random_seed" "0"
      if [ $? -ne 0 ]; then
        popd > /dev/null 2>&1
        rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
        exit 1;
      fi

      if [ "$ALGORITHM" != "random" ] && [ "$ALGORITHM" != "randomsearch" ] && [ "$ALGORITHM" != "geneticalgorithm" ]; then
        break
      fi
    done
  fi

popd > /dev/null 2>&1

# ------------------------------- Collect and compress data & Clean up

echo ""
echo "[INFO] Collect data & Clean up"

pushd . > /dev/null 2>&1
cd "$LOCAL_TMP_DIR"
  echo "DONE!"

  # get log file so that it can also be in the .tar.bz2 file
  cp -f "$DATA_DIR/log.txt" "prioritisation-data/" > /dev/null 2>&1

  # zip everything
  if [ "$ALGORITHM" == "schwa" ]; then
    if [ "$BUG_PREDICTION_TYPE" == "default" ]; then
      zip_filename="$PID-$BID-$ALGORITHM-$SECONDARY_OBJECTIVE-$TOOL-prioritisation-data.tar.bz2"
    elif [ "$BUG_PREDICTION_TYPE" == "best" ]; then
      zip_filename="$PID-$BID-best-$ALGORITHM-$SECONDARY_OBJECTIVE-$TOOL-prioritisation-data.tar.bz2"
    elif [ "$BUG_PREDICTION_TYPE" == "n_commits" ]; then
      zip_filename="$PID-$BID-$BUG_PREDICTION_NUM_COMMITS-commits-$ALGORITHM-$SECONDARY_OBJECTIVE-$TOOL-prioritisation-data.tar.bz2"
    fi
  else
    zip_filename="$PID-$BID-$ALGORITHM-$TOOL-prioritisation-data.tar.bz2"
  fi

  tar -jcvf "$zip_filename" "prioritisation-data/" 
  if [ $? -ne 0 ]; then
    echo "[ERROR] It was not possible to compress directory '$LOCAL_TMP_DIR/prioritisation-data/'!"

    echo "[INFO] Copying all files from local '$LOCAL_TMP_DIR/prioritisation-data' to remote '$DATA_DIR' so that anyone can debug them"
    cp -Rv prioritisation-data/* "$DATA_DIR/"

    popd > /dev/null 2>&1
    rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
    exit 1;
  fi

  cp -v "$zip_filename" "$DATA_DIR/../"
  # remove remote directory, as everything went fine
  rm -rf "$DATA_DIR" > /dev/null 2>&1

popd > /dev/null 2>&1

# do not leave anything behind
rm -rf "$tmp_dir" "$LOCAL_TMP_DIR" > /dev/null 2>&1

exit 0;

# EOF

