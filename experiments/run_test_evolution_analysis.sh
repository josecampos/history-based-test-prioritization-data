#!/bin/bash
#
# --------------------------------------------------------------------
# This script submits as many jobs as the number of projects * number
# of bugs. Each job runs the d4j-test-evolution command. The outcome
# is written to:
#   ../data/_project_/_bug_id_/manual/_project_-_bug_id_-manual-test-evolution-data.tar.bz2
#
# Usage:
# run_test_evolution_analysis.sh
#
# Requirements:
#   Execution of tools/get_tools.sh script.
# 
# --------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../experiments/utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether ANT_HOME exists
ANT_HOME="$SCRIPT_DIR/../tools/apache-ant"
if [ ! -d "$ANT_HOME" ]; then
  die "Could not find java 'apache-ant' directory! Did you run 'tools/get_tools.sh'?"
else
  export ANT_HOME="$ANT_HOME"
fi

# Check whether D4J_HOME exists
D4J_HOME="$SCRIPT_DIR/../tools/defects4j"
if [ ! -d "$D4J_HOME" ]; then
  die "Could not find 'Defects4J' home directory! Did you run 'tools/get_tools.sh'?"
else
  export D4J_HOME="$D4J_HOME"
fi

# Check whether JAVA_7_HOME exists
JAVA_7_HOME="$SCRIPT_DIR/../tools/jdk-7"
if [ ! -d "$JAVA_7_HOME" ]; then
  die "Could not find java '$JAVA_7_HOME' directory! Did you run 'tools/get_tools.sh'?"
else
  export JAVA_7_HOME="$JAVA_7_HOME"
fi

USAGE="Usage: $0"
[ $# -eq 0 ] || die "$USAGE";

DATA_DIR="$SCRIPT_DIR/../data"
if [ ! -d "$DATA_DIR" ]; then
  die "Directory '$DATA_DIR' does not exist!"
fi

TOOL="manual" # TODO adapt script in case of automatically generated test cases

# --------------------------------------------------------------- Main

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"

  # Get all bugs
  bids=$(cut -f1 -d',' $dir_project/commit-db)

  # Iterate over all bugs
  for bid in $bids; do
    root_dir="$DATA_DIR/$pid/$bid"

    ##
    # have we got test-evolution-data?
    test_evolution_data_zip="$root_dir/$TOOL/$pid-$bid-$TOOL-test-evolution-data.tar.bz2"
    if [ -s "$test_evolution_data_zip" ]; then
      log_file_tmp="/tmp/log_file_tmp_$pid-$bid-$TOOL-$$.txt"
      tar -xf "$test_evolution_data_zip" test-evolution-data/log.txt -O > "$log_file_tmp"
      if [ $? -ne 0 ]; then
        echo "[ERROR] It was not possible to extract 'log.txt' from '$test_evolution_data_zip', therefore a sanity-check on $pid-$bid could not be performed."
      else
        if grep -q "^DONE\!$" "$log_file_tmp" && ! grep -q " No space left on device$" "$log_file_tmp"; then
          # skip pid-bid if it has completed successfully
          rm -f "$log_file_tmp"
          continue;
        fi
      fi

      rm -f "$log_file_tmp"
    fi

    test_evolution_data_dir="$root_dir/$TOOL/test-evolution-data"
    rm -rf "$test_evolution_data_dir"; mkdir -p "$test_evolution_data_dir"
    log_file="$test_evolution_data_dir/log.txt"

    echo "$pid-$bid"
    echo "$pid-$bid" > "$log_file"

    pushd . > /dev/null 2>&1
    cd "$SCRIPT_DIR"
      if _am_I_a_cluster; then
        qsub -V -N "_$bid-$pid-run_test_evolution_analysis" -l h_rt=08:00:00 -l rmem=4G -e "$log_file" -o "$log_file" -j y "_run_test_evolution_analysis.sh" $pid $bid $TOOL $test_evolution_data_dir;
      else
        timeout --signal=KILL "8h" bash "_run_test_evolution_analysis.sh" $pid $bid $TOOL $test_evolution_data_dir > "$log_file" 2>&1 &
        _register_background_process $!;
        _can_more_jobs_be_submitted;
      fi
    popd > /dev/null 2>&1

  done
done

# It might be the case that there are still jobs running
_wait_for_jobs;

echo "All jobs submitted!"

# EOF

