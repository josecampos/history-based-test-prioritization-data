#!/bin/bash

PWD=`pwd`

export MALLOC_ARENA_MAX=1 # Iceberg's requirement
export TZ='America/Los_Angeles' # some D4J's requires this specific TimeZone

export _JAVA_OPTIONS="-Xmx4096M -XX:MaxHeapSize=2048M"
export MAVEN_OPTS="-Xmx1024M"
export ANT_OPTS="-Xmx4096M -XX:MaxHeapSize=2048M"

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

NUM_CPUS=-1
PROCESS_IDS=()
CORE_INDEX=0

##
# Prints error message to the stdout and exit.
##
die() {
  echo "$@" >&2
  exit 1
}

##
#
##
_assert() {
  local USAGE="Usage: _assert <condition> <\$LINENO>"
  if [ "$#" != 2 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local condition="$1"
  local lineno="$2"

  if [ ! $condition ]; then
    echo "Assertion \"$condition\" failed" >&2
    die "File \"$0\", line $lineno" # Give name of file and line number
  fi

  return 0
}

##
# Checks whether script has been executed on a cluster, e.g., "Son of
# Grid Engine" (SGE) or not. Return true ('0') if yes, otherwise it
# returns false ('1')
##
_am_I_a_cluster() {
  if ! module > /dev/null 2>&1; then
    return 1 # false
  fi

  return 0 # true
}

##
# Determines the number of cpus/cores available
##
_how_many_cpus() {
  ##num_cpus=$(awk -F: '/^physical/ && !ID[$2] { P++; ID[$2]=1 }; /^cpu cores/ { CORES=$2 }; END { print CORES*P }' /proc/cpuinfo)
  ##if [ $? -ne 0 ]; then
  ##  num_cpus=$(getconf _NPROCESSORS_ONLN)
  ##  if [ $? -ne 0 ]; then
  ##    return 1
  ##  fi
  ##fi

  #echo "$num_cpus"
  echo "1"
  return 0
}

##
# Keeps record of all processes that are sent to background to run
##
_register_background_process() {
  local USAGE="Usage: _register_background_process <pid>"
  if [ "$#" != 1 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local background_process_pid=$1

  CORE_INDEX=$((CORE_INDEX+1))
  PROCESS_IDS+=($background_process_pid)

  return 0
}

##
# Waits until all background jobs finish
##
_wait_for_jobs() {
  echo "[DEBUG] Waiting for pids ${PROCESS_IDS[@]} ..." >&2
  date >&2
  wait "${PROCESS_IDS[@]}" >&2
  date >&2
  echo "[DEBUG] Done ..." >&2

  return 0
}

##
# If no more jobs can be executed, because the maximum number of jobs
# in parallel has been reached, this function waits for the running
# jobs to finish
##
_can_more_jobs_be_submitted() {
  if [ "$NUM_CPUS" -eq "-1" ]; then
    # init number of cpus available
    NUM_CPUS=$(_how_many_cpus)
    if [ $? -ne 0 ]; then
      return 1
    fi
  fi

  if [ "$CORE_INDEX" -eq "$NUM_CPUS" ]; then
    _wait_for_jobs;

    PROCESS_IDS=() # reset list of pids
    CORE_INDEX=0 # reset counter
  fi

  return 0
}

##
# Returns the minimum value of a list of values.
##
_min() {
  local USAGE="Usage: _min <list of integers, e.g., '284 859'>"
  if [ "$#" != 1 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local n=$1

  # If n is empty then give up
  size=${#n[*]}
  if [[ "$size" -eq "0" ]]; then
    echo "[ERROR] Array is empty!"
    return 1
  elif [[ "$size" -eq "1" ]]; then
    echo "${n[0]}"
    return 0
  fi

  min="$(str_list_of_values=$n python - <<END
from os import environ
str_list_of_values = environ.get('str_list_of_values', 'missing!')

arr = []
for value in str_list_of_values.split(" "):
  arr.append(int(value))

print('%.2f') %(min(arr))
END
)"

  echo "$min"
  return 0
}

##
# Returns the maximum value of a list of values.
##
_max() {
  local USAGE="Usage: _max <list of integers, e.g., '284 859'>"
  if [ "$#" != 1 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local n=$1

  # If n is empty then give up
  size=${#n[*]}
  if [[ "$size" -eq "0" ]]; then
    echo "[ERROR] Array is empty!"
    return 1
  elif [[ "$size" -eq "1" ]]; then
    echo "${n[0]}"
    return 0
  fi

  max="$(str_list_of_values=$n python - <<END
from os import environ
str_list_of_values = environ.get('str_list_of_values', 'missing!')

arr = []
for value in str_list_of_values.split(" "):
  arr.append(int(value))

print('%.2f') %(max(arr))
END
)"

  echo "$max"
  return 0
}

##
# Computes the mean of a list of integers.
##
_mean() {
  local USAGE="Usage: _mean <list of integers, e.g., '284 859'>"
  if [ "$#" != 1 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local n=$1

  # If n is empty then give up
  size=${#n[*]}
  if [[ "$size" -eq "0" ]]; then
    echo "[ERROR] Array is empty!"
    return 1
  elif [[ "$size" -eq "1" ]]; then
    echo "${n[0]}"
    return 0
  fi

  mean="$(str_list_of_values=$n python - <<END
from os import environ
str_list_of_values = environ.get('str_list_of_values', 'missing!')

arr = []
for value in str_list_of_values.split(" "):
  arr.append(int(value))

mean = float(sum(arr)) / max(len(arr), 1)
print('%.2f') %(mean)
END
)"

  echo "$mean"
  return 0
}

##
# Computes the median of a list of integers.
##
_median() {
  local USAGE="Usage: _median <list of integers, e.g., '284 859'>"
  if [ "$#" != 1 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local n=$1

  # If n is empty then give up
  size=${#n[*]}
  if [[ "$size" -eq "0" ]]; then
    echo "[ERROR] Array is empty!"
    return 1
  elif [[ "$size" -eq "1" ]]; then
    echo "${n[0]}"
    return 0
  fi

  median="$(str_list_of_values=$n python - <<END
from os import environ
str_list_of_values = environ.get('str_list_of_values', 'missing!')

arr = []
for value in str_list_of_values.split(" "):
  arr.append(int(value))

length = len(arr)
if length % 2 == 1:
  median = sorted(arr)[length//2]
else:
  median = sum(sorted(arr)[length//2-1:length//2+1])/2.0
print('%.2f') %(median)
END
)"

  echo "$median"
  return 0
}

##
# Checkouts a D4J's project-bug.
##
_checkout() {
  local USAGE="Usage: _checkout <pid> <bid> <fixed (f) or buggy (b)>"
  if [ "$#" != 3 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local pid="$1"
  local bid="$2"
  local version="$3" # either b or f

  local output_dir="/tmp/$USER/history-based-test-case-prioritization/_$$-$pid-$bid"
  rm -rf "$output_dir"; mkdir -p "$output_dir"
  "$D4J_HOME/framework/bin/defects4j" checkout -p "$pid" -v "${bid}$version" -w "$output_dir"
  if [ $? -ne 0 ]; then
    if _am_I_a_cluster; then
      rm -rf "$output_dir"

      # trying another directory
      output_dir="/scratch/$USER/history-based-test-case-prioritization/_$$-$pid-$bid"
      rm -rf "$output_dir"; mkdir -p "$output_dir"
      "$D4J_HOME/framework/bin/defects4j" checkout -p "$pid" -v "${bid}$version" -w "$output_dir" || return 1
    else
      return 1
    fi
  fi

  echo "$output_dir"
  return 0
}

##
# Compiles a D4J's project-bug.
##
_compile() {
  local USAGE="Usage: _compile <project dir>"
  if [ "$#" != 1 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local project_dir="$1"

  if [[ $project_dir == *"-Mockito-"* ]]; then
    rm -f "$project_dir/buildSrc/src/test/groovy/testutil/OfflineCheckerTest.groovy" # this test case may cause the compilation to fail

    echo "[DEBUG] Mockito project identified, export '$project_dir/.gradle-local-home' as the 'GRADLE_USER_HOME'" >&2
    export GRADLE_USER_HOME="$project_dir/.gradle-local-home"
  fi

  pushd . > /dev/null 2>&1
  cd "$project_dir"
    "$D4J_HOME/framework/bin/defects4j" compile || return 1
  popd > /dev/null 2>&1

  return 0
}

##
# Returns the test-classpath of a D4J's project-bug.
##
_get_classpath() {
  local USAGE="Usage: _get_classpath <project dir>"
  if [ "$#" != 1 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local project_dir="$1"

  pushd . > /dev/null 2>&1
  cd "$project_dir"
    local CP=$("$D4J_HOME/framework/bin/defects4j" export -p cp.test)

    # make sure a junit.jar is on the classpath
    local validCP="$D4J_HOME/framework/projects/lib/junit-4.11.jar"
    # remove invalid junit.jar files in the classpath
    for entry in $(echo "$CP" | tr ":" "\n"); do
      if [ -e "$entry" ]; then
        validCP="$validCP:$entry"
      fi
    done

    # add an extra dependency to Mockito's bugs. although it should
    # just be a compilation dependency, there are several classes that
    # depend on. therefore, it should also be on the classpath.
    if [[ $project_dir == *"-Mockito-"* ]]; then
      if [ -f "compileLib/byte-buddy-0.2.1.jar" ]; then
        validCP="$validCP:$project_dir/compileLib/byte-buddy-0.2.1.jar"
      elif [ -f "compileLib/byte-buddy-0.6.8.jar" ]; then
        validCP="$validCP:$project_dir/compileLib/byte-buddy-0.6.8.jar"
      fi
    elif [[ $project_dir == *"-Closure-"* ]]; then
      if [ -f "lib/ant.jar" ]; then
        validCP="$validCP:$project_dir/lib/ant.jar"
      fi
    fi

    CP="$validCP"
  popd > /dev/null 2>&1

  if [[ $project_dir == *"-Mockito-"* ]]; then
    # D4J export command for Mockito, removes the content of
    # test-classes directory. so, we need to re-compiled it. more
    # info in here: https://github.com/rjust/defects4j/issues/39
    _compile "$project_dir"
    if [ $? -ne 0 ]; then
      return 1
    fi
  fi

  echo "$CP"
  return 0
}

##
# Returns the target directory of classes (relative to working directory).
##
_get_classes_target_directory() {
  local USAGE="Usage: _get_classes_target_directory <project dir>"
  if [ "$#" != 1 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local project_dir="$1"

  pushd . > /dev/null 2>&1
  cd "$project_dir"
    local dir_bin_classes=""
    dir_bin_classes=$("$D4J_HOME/framework/bin/defects4j" export -p dir.bin.classes)
    if [ $? -ne 0 ]; then
      return 1
    fi
  popd > /dev/null 2>&1

  echo "$dir_bin_classes"
  return 0
}

##
# Returns the target directory of test classes (relative to working directory).
##
_get_test_classes_target_directory() {
  local USAGE="Usage: _get_test_classes_target_directory <project dir>"
  if [ "$#" != 1 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local project_dir="$1"

  pushd . > /dev/null 2>&1
  cd "$project_dir"
    local dir_bin_test_classes=""
    dir_bin_test_classes=$("$D4J_HOME/framework/bin/defects4j" export -p dir.bin.tests)
    if [ $? -ne 0 ]; then
      return 1
    fi
  popd > /dev/null 2>&1

  echo "$dir_bin_test_classes"
  return 0
}

##
# Runs D4J's command test-evolution and collect data.
##
_test_evolution() {
  local USAGE="Usage: _test_evolution <checkout dir> <data dir>"
  if [ "$#" != 2 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local checkout_dir="$1"
  local data_dir="$2"

  pushd . > /dev/null 2>&1
  cd "$checkout_dir"
    # Run the 'test-evolution' in the current directory
    "$D4J_HOME/framework/bin/defects4j" test-evolution -w . || return 1
  popd > /dev/null 2>&1

  ## Sanity checks

  local revisions_dir="$checkout_dir/.revisions"
  local data_file="$checkout_dir/test_evolution.csv"

  if [ ! -d "$revisions_dir" ]; then
    echo "[ERROR] There is not any '$revisions_dir'!" >&2
    return 1
  fi

  if [ ! -s "$data_file" ]; then
    echo "[ERROR] There is not any '$data_file' file or it is empty!" >&2
    return 1
  fi

  local num_rows_data_file=$(cat "$data_file" | wc -l)
  if [ "$num_rows_data_file" -eq "1" ]; then
    echo "[ERROR] '$data_file' file does not contain any data!" >&2
    return 1
  fi

  mv -v -f "$revisions_dir" "$data_dir" >&2 || return 1
  mv -v -f "$data_file" "$data_dir" >&2 || return 1

  return 0
}

# EOF

