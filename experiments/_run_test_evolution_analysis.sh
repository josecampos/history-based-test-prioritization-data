#!/bin/bash

#SCRIPT_DIR=$(cd `dirname $0` && pwd)
SCRIPT_DIR=`pwd`
source "$SCRIPT_DIR/../experiments/utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether JAVA_7_HOME is set
[ "$JAVA_7_HOME" != "" ] || die "[ERROR] JAVA_7_HOME is not set!"
# Check whether ANT_HOME is set
[ "$ANT_HOME" != "" ] || die "[ERROR] ANT_HOME is not set!"
# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "[ERROR] D4J_HOME is not set!"

USAGE="Usage: $0 <pid> <bid> <tool: manual, evosuite, or randoop> <data dir>"
[ $# -eq 4 ] || die "$USAGE";
PID="$1"
BID="$2"
TOOL="$3"
DATA_DIR="$4"

LOCAL_TMP_DIR="/tmp/$USER/_run_test_evolution_analysis-$PID-$BID-$$"
rm -rf "$LOCAL_TMP_DIR"; mkdir -p "$LOCAL_TMP_DIR"

LOCAL_DATA_DIR="$LOCAL_TMP_DIR/test-evolution-data"
mkdir -p "$LOCAL_DATA_DIR"

# --------------------------------------------------------------- Main

export PATH="$JAVA_7_HOME/bin:$ANT_HOME/bin:$PATH"

echo "PID: $$"
hostname

echo ""
echo "[INFO] Checkout $PID-${BID}b"
tmp_dir=$(_checkout "$PID" "$BID" "b");
if [ $? -ne 0 ]; then
  echo "[ERROR] Checkout of the BUGGY version has failed!"
  rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
  exit 1;
fi

echo ""
echo "[INFO] Run test evolution on $PID-${BID}b"
_test_evolution "$tmp_dir" "$LOCAL_DATA_DIR"
if [ $? -ne 0 ]; then
  echo "[ERROR] Test evolution $PID-${BID}b has failed!"
  rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
  exit 1;
fi

# ------------------------------- Collect and compress data & Clean up

echo ""
echo "[INFO] Collect data & Clean up"

pushd . > /dev/null 2>&1
cd "$LOCAL_TMP_DIR"
  echo "DONE!"

  # get log file so that it can also be in the .tar.bz2 file
  cp -f "$DATA_DIR/log.txt" "test-evolution-data/" > /dev/null 2>&1

  zip_filename="$PID-$BID-$TOOL-test-evolution-data.tar.bz2"
  tar -jcvf "$zip_filename" "test-evolution-data/" 
  if [ $? -ne 0 ]; then
    echo "[ERROR] It was not possible to compress directory '$LOCAL_TMP_DIR/test-evolution-data/'!"

    echo "[INFO] Copying all files from local '$LOCAL_TMP_DIR/test-evolution-data' to remote '$DATA_DIR' so that anyone can debug them"
    cp -Rv test-evolution-data/* "$DATA_DIR/"

    popd > /dev/null 2>&1
    rm -rf "$tmp_dir" "$LOCAL_TMP_DIR"
    exit 1;
  fi

  cp -v "$zip_filename" "$DATA_DIR/../"
  # remove remote directory, as everything went fine
  rm -rf "$DATA_DIR" > /dev/null 2>&1

popd > /dev/null 2>&1

# do not leave anything behind
rm -rf "$tmp_dir" "$LOCAL_TMP_DIR" > /dev/null 2>&1

exit 0;

# EOF

