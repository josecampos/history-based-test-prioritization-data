#!/bin/bash
#
# --------------------------------------------------------------------
# This script submits as many jobs as the number of projects * number
# of bugs. Each job runs Schwa on the buggy version of a particular
# D4J's project-bug.
#
# Usage:
# bug_prediction.sh <DATA DIR> [NUM COMMITS FILE]
#
# Parameters:
#   <DATA DIR>   Directory to which the coverage data will be
#                generated. The script will create the following
#                directory:
#                   DATA DIR/project/bug_id/bug-prediction-data
#
# Requirements:
#   Execution of tools/get_tools.sh script.
# --------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether ANT_HOME exists
ANT_HOME="$SCRIPT_DIR/../../tools/apache-ant"
if [ ! -d "$ANT_HOME" ]; then
  die "Could not find java 'apache-ant' directory! Did you run 'tools/get_tools.sh'?"
else
  export ANT_HOME="$ANT_HOME"
fi

# Check whether D4J_HOME exists
D4J_HOME="$SCRIPT_DIR/../../tools/defects4j"
if [ ! -d "$D4J_HOME" ]; then
  die "Could not find 'Defects4J' home directory! Did you run 'tools/get_tools.sh'?"
else
  export D4J_HOME="$D4J_HOME"
fi

USAGE="Usage: $0 <data dir> [n commits file]"
if [ $# -ne 1 ] && [ $# -ne 2 ]; then
  die "$USAGE";
fi

DATA_DIR="$1"
if [ ! -d "$DATA_DIR" ]; then
  mkdir -p "$DATA_DIR"
fi

NUM_COMMITS_FILE="-1"
if [ $# -eq 2 ]; then
  NUM_COMMITS_FILE="$2"
  if [ ! -s "$NUM_COMMITS_FILE" ]; then
    die "'$NUM_COMMITS_FILE' does not exist"
  fi
fi

# Tuning values
BEST_PARAMETERS_FILE="$SCRIPT_DIR/bug_prediction_best_parameters.txt"
if [ ! -s "$BEST_PARAMETERS_FILE" ]; then
  die "Could not find '$BEST_PARAMETERS_FILE' file!"
fi

REVISIONS_WEIGHT=$(grep "^REVISIONS_WEIGHT=" "$BEST_PARAMETERS_FILE" | cut -f2 -d'=')
FIXES_WEIGHT=$(grep "^FIXES_WEIGHT=" "$BEST_PARAMETERS_FILE" | cut -f2 -d'=')
AUTHORS_WEIGHT=$(grep "^AUTHORS_WEIGHT=" "$BEST_PARAMETERS_FILE" | cut -f2 -d'=')
TIME_RANGE=$(grep "^TIME_RANGE=" "$BEST_PARAMETERS_FILE" | cut -f2 -d'=')

# --------------------------------------------------------------- Main

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"

  # Get all bugs
  bids=$(cut -f1 -d',' $dir_project/commit-db)

  # Iterate over all bugs
  for bid in $bids; do
    root_dir="$DATA_DIR/$pid/$bid"

    if [ -f "$NUM_COMMITS_FILE" ]; then
      if ! grep -q "^$pid,$bid," "$NUM_COMMITS_FILE"; then
        continue
      fi
      n_commits=$(grep "^$pid,$bid," "$NUM_COMMITS_FILE" | cut -f3 -d',')
      if [ "$n_commits" -eq "0" ]; then
        continue
      fi
    fi

    bug_prediction_data_zip="$root_dir/$pid-$bid-bug-prediction-data.tar.bz2"
    if [ "$n_commits" -ne "-1" ]; then
      bug_prediction_data_zip="$root_dir/$pid-$bid-$n_commits-commits-bug-prediction-data.tar.bz2"
    fi
    if [ -s "$bug_prediction_data_zip" ]; then
      log_file_tmp="/tmp/log_file_tmp_$USER-$pid-$bid-$$.txt"
      >"$log_file_tmp"

      tar -xf "$bug_prediction_data_zip" bug-prediction-data/log.txt -O > "$log_file_tmp"
      if [ $? -ne 0 ]; then
        echo "[ERROR] It was not possible to extract 'log.txt' from '$bug_prediction_data_zip', therefore a sanity-check on $pid-$bid could not be performed."
      else
        if grep -q "^DONE\!$" "$log_file_tmp" && ! grep -q " No space left on device$" "$log_file_tmp"; then
          # skip pid-bid if it has completed successfully
          rm -f "$log_file_tmp"
          continue;
        fi
      fi

      rm -f "$log_file_tmp"
    fi

    bug_prediction_data_dir="$root_dir/bug-prediction-data"
    rm -rf "$bug_prediction_data_dir"
    if [ $? -ne 0 ]; then
      echo "[ERROR] You might not have write permissions to remove '$bug_prediction_data_dir'!"
      continue
    fi
    mkdir -p "$bug_prediction_data_dir"
    if [ $? -ne 0 ]; then
      echo "[ERROR] You might not have write permissions to create '$bug_prediction_data_dir'!"
      continue
    fi

    echo "$pid-$bid"
    log_file="$bug_prediction_data_dir/log.txt"
    echo "$pid-$bid" > "$log_file"
    if [ $? -ne 0 ]; then
      echo "[ERROR] You might not have write permissions to write to '$log_file'!"
      continue
    fi

    pushd . > /dev/null 2>&1
    cd "$SCRIPT_DIR"
      if _am_I_a_cluster; then
        qsub -V -N "_$bid-$pid-bug-prediction" -l h_rt=8:00:00 -l rmem=4G -e "$log_file" -o "$log_file" -j y "_bug_prediction.sh" $pid $bid $bug_prediction_data_dir "$REVISIONS_WEIGHT" "$FIXES_WEIGHT" "$AUTHORS_WEIGHT" "$TIME_RANGE" "$n_commits" true;
      else
        timeout --signal=KILL 8h bash "_bug_prediction.sh" $pid $bid $bug_prediction_data_dir "$REVISIONS_WEIGHT" "$FIXES_WEIGHT" "$AUTHORS_WEIGHT" "$TIME_RANGE" "$n_commits" true > "$log_file" 2>&1 &
        _register_background_process $!;
        _can_more_jobs_be_submitted;
      fi
    popd > /dev/null 2>&1
  done
done

# It might be the case that there are still jobs running
_wait_for_jobs;

echo "All jobs submitted!"

# EOF

