#!/bin/bash
#
# --------------------------------------------------------------------
# This script creates the perfect bug-prediction possible for each
# D4J bug. I.e., it generates a json file (as Schwa) but with all
# faulty files/classes with a likelihood of being faulty of 1.0, and
# compress all data to a file called:
#   ../../data/$pid/$bid/$pid-$bid-perfect-bug-prediction-data.tar.bz2
#
# Usage:
# perfect_bug_prediction.sh
#
# Requirements:
#   Execution of tools/get_tools.sh script.
# --------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether ANT_HOME exists
ANT_HOME="$SCRIPT_DIR/../../tools/apache-ant"
if [ ! -d "$ANT_HOME" ]; then
  die "Could not find java 'apache-ant' directory! Did you run 'tools/get_tools.sh'?"
else
  export ANT_HOME="$ANT_HOME"
fi

# Check whether D4J_HOME exists
D4J_HOME="$SCRIPT_DIR/../../tools/defects4j"
if [ ! -d "$D4J_HOME" ]; then
  die "Could not find 'Defects4J' home directory! Did you run 'tools/get_tools.sh'?"
else
  export D4J_HOME="$D4J_HOME"
fi

USAGE="Usage: $0"
[ $# -eq 0 ] || die "$USAGE";

DATA_DIR="$SCRIPT_DIR/../../data"
if [ ! -d "$DATA_DIR" ]; then
  die "Directory '$DATA_DIR' does not exist!"
fi

# ---------------------------------------------------------------- aux

## Schwa format
#{
#    "children": [
#        {
#            "prob": "1.0",
#            "size": "1.0",
#            "authors": 0,
#            "path": "tests/org/jfree/chart/annotations/junit/AnnotationsPackageTests.java",
#            "type": "file",
#            "fixes": 0,
#            "revisions": 0,
#            "children": [
#                {
#                  "prob": "1.0",
#                  "size": "1.0",
#                  "authors": 0,
#                  "type": "class",
#                  "fixes": 0,
#                  "revisions": 0,
#                  "children": [],
#                  "name": "AnnotationsPackageTests"
#                }
#            ],
#            "name": "AnnotationsPackageTests.java"
#        }
#    ],
#    "name": "root"
#}

# --------------------------------------------------------------- Main

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"

  # Get all bugs
  bids=$(cut -f1 -d',' $dir_project/commit-db)

  # Iterate over all bugs
  for bid in $bids; do
    root_dir="$DATA_DIR/$pid/$bid"

    perfect_bug_prediction_data_dir_name="bug-prediction-data"
    perfect_bug_prediction_data_dir="$root_dir/$perfect_bug_prediction_data_dir_name"
    rm -rf "$perfect_bug_prediction_data_dir"; mkdir -p "$perfect_bug_prediction_data_dir"

    zip_file_name="$pid-$bid-perfect-bug-prediction-data.tar.bz2"
    zip_file_path="$perfect_bug_prediction_data_dir/$zip_file_name"
    rm -f "$zip_file_path"

    log_file="$perfect_bug_prediction_data_dir/log.txt"
    echo "$pid-${bid}b" > "$log_file"

    json_unpretty_file="$perfect_bug_prediction_data_dir/$pid-${bid}b.json"
    json_pretty_file="$perfect_bug_prediction_data_dir/$pid-${bid}b-pretty-print.json"

    echo "{" > "$json_pretty_file"
    echo "    \"children\": [" >> "$json_pretty_file"

    first=0
    for modified_file_path in $(grep "^+++ " "$D4J_HOME/framework/projects/$pid/patches/$bid.src.patch" | sed 's/^+++ //' | sed 's/^b\///' | cut -f1); do

      echo "Modified file path: $modified_file_path" >> "$log_file"
      file_name=$(echo "${modified_file_path##*/}")
      echo "Modified file: $file_name" >> "$log_file"
      class_name=$(echo "$file_name" | cut -f1 -d'.')
      echo "Buggy class: $class_name" >> "$log_file"

      if [ "$first" -eq "0" ]; then
        echo "        {" >> "$json_pretty_file"
      else
        echo "        ,{" >> "$json_pretty_file"
      fi

      # file
      echo "            \"prob\": \"1.0\"," >> "$json_pretty_file"
      echo "            \"size\": \"1.0\"," >> "$json_pretty_file"
      echo "            \"authors\": 0," >> "$json_pretty_file"
      echo "            \"path\": \"$modified_file_path\"," >> "$json_pretty_file"
      echo "            \"type\": \"file\"," >> "$json_pretty_file"
      echo "            \"fixes\": 0," >> "$json_pretty_file"
      echo "            \"revisions\": 0," >> "$json_pretty_file"
      # class
      echo "            \"children\": [" >> "$json_pretty_file"
      echo "                {" >> "$json_pretty_file"
      echo "                  \"prob\": \"1.0\"," >> "$json_pretty_file"
      echo "                  \"size\": \"1.0\"," >> "$json_pretty_file"
      echo "                  \"authors\": 0," >> "$json_pretty_file"
      echo "                  \"type\": \"class\"," >> "$json_pretty_file"
      echo "                  \"fixes\": 0," >> "$json_pretty_file"
      echo "                  \"revisions\": 0," >> "$json_pretty_file"
      echo "                  \"children\": []," >> "$json_pretty_file"
      echo "                  \"name\": \"$class_name\"" >> "$json_pretty_file"
      echo "                }" >> "$json_pretty_file"
      echo "            ]," >> "$json_pretty_file"
      echo "            \"name\": \"$file_name\"" >> "$json_pretty_file"

      echo "        }" >> "$json_pretty_file"

      first=1
    done

    echo "    ]," >> "$json_pretty_file"
    echo "    \"name\": \"root\"" >> "$json_pretty_file"
    echo "}" >> "$json_pretty_file"

    # unprettify file
    tr -d '[:space:]' < "$json_pretty_file" > "$json_unpretty_file"

    echo "DONE!" >> "$log_file"

    # Collect data
    pushd . > /dev/null 2>&1
    cd "$root_dir"
      tar -jcvf "$pid-$bid-perfect-bug-prediction-data.tar.bz2" "$perfect_bug_prediction_data_dir_name/" 
      if [ $? -ne 0 ]; then
        die "[ERROR] It was not possible to compress directory '$perfect_bug_prediction_data_dir'!"
      fi
    popd > /dev/null 2>&1

    # Clean up
    rm -rf "$perfect_bug_prediction_data_dir"
  done
done

echo "DONE!"

# EOF

