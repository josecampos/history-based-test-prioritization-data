#!/bin/bash
#
# --------------------------------------------------------------------
# This script submits as many jobs as the number of different
# configurations to instantiate Schwa * number of a subset of D4J's
# bugs. Each job runs Schwa on the buggy version of a particular D4J's
# project-bug with a single and unique configuration of Schwa.
#
# Usage:
# bug_prediction_tuning.sh <DATA DIR> <PROJECT BUG_ID>
#
# Parameters:
#   <DATA DIR>   Directory to which the coverage data will be
#                generated. The script will create the following
#                directory:
#                   DATA DIR/project/bug_id/bug-prediction-data/tuning/<configuration name>
#
# Requirements:
#   Execution of tools/get_tools.sh script.
# --------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether ANT_HOME exists
ANT_HOME="$SCRIPT_DIR/../../tools/apache-ant"
if [ ! -d "$ANT_HOME" ]; then
  die "Could not find java 'apache-ant' directory! Did you run 'tools/get_tools.sh'?"
else
  export ANT_HOME="$ANT_HOME"
fi

# Check whether D4J_HOME exists
D4J_HOME="$SCRIPT_DIR/../../tools/defects4j"
if [ ! -d "$D4J_HOME" ]; then
  die "Could not find 'Defects4J' home directory! Did you run 'tools/get_tools.sh'?"
else
  export D4J_HOME="$D4J_HOME"
fi

USAGE="Usage: $0 <data dir> [project bug_id]"
if [ $# -ne 1 ] && [ $# -ne 3 ]; then
  die "$USAGE";
fi

DATA_DIR="$1"
if [ ! -d "$DATA_DIR" ]; then
  mkdir -p "$DATA_DIR"
fi

PROJECT=""
BUG_ID=""
if [ $# -eq 3 ]; then
  PROJECT="$2"
  BUG_ID="$3"
fi

BUGS_STRATIFIED_FILE="$SCRIPT_DIR/../../defects4J/bugs.stratified.txt"
if [ ! -f "$BUGS_STRATIFIED_FILE" ]; then
  die "There is not any '$BUGS_STRATIFIED_FILE' file!";
fi

# --------------------------------------------------------------- Main

tail -n +2 "$BUGS_STRATIFIED_FILE" | while read -r project_bug; do
  pid=$(echo "$project_bug" | cut -f1 -d',')
  bid=$(echo "$project_bug" | cut -f2 -d',')

  if [ "$PROJECT" != "" ] && [ "$BUG_ID" != "" ]; then
    if [ "$pid" != "$PROJECT" ] || [ "$bid" != "$BUG_ID" ]; then
      continue;
    fi
  fi

  # iterate over all (possible) configurations
  for revisions_weight in 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0; do
    for fixes_weight in 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0; do
      for authors_weight in 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0; do

        # if it's not a valid configuration do not even try to run it
        sum_features_weights=$(echo "$revisions_weight + $fixes_weight + $authors_weight" | bc)
        if [ $(echo "$sum_features_weights > 1.0" | bc -l) -eq 1 ] || [ $(echo "$sum_features_weights < 1.0" | bc -l) -eq 1 ]; then
          continue;
        fi

        for time_range in 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0; do
          bid_data_dir="$DATA_DIR/$pid/$bid/bug-prediction-data/tuning/revisions_weight_$revisions_weight-fixes_weight_$fixes_weight-authors_weight_$authors_weight-time_range_$time_range"
          mkdir -p "$bid_data_dir"
          log_file="$bid_data_dir/log.txt"

          if [ -f "$log_file" ]; then
            if grep -q "^DONE\!$" "$log_file" && ! grep -q " No space left on device$" "$log_file"; then
              continue;
            fi
          fi

          echo "$pid-$bid"
          echo "$pid-$bid" > "$log_file"
          if [ $? -ne 0 ]; then
            echo "[ERROR] You might not have write permissions to write to '$log_file'!"
            continue
          fi

          echo "Revisions weight: $revisions_weight" >> "$log_file"
          echo "Fixes weight: $fixes_weight" >> "$log_file"
          echo "Authors weight: $authors_weight" >> "$log_file"
          echo "Time range: $time_range" >> "$log_file"

          pushd . > /dev/null 2>&1
          cd "$SCRIPT_DIR"
            if _am_I_a_cluster; then
              qsub -V -N "_$bid-$pid-bug-prediction-$revisions_weight-$fixes_weight-$authors_weight-$time_range" -l h_rt=8:00:00 -l rmem=4G -e "$log_file" -o "$log_file" -j y "_bug_prediction.sh" $pid $bid $bid_data_dir "$revisions_weight" "$fixes_weight" "$authors_weight" "$time_range" "-1" false;
            else
              timeout --signal=KILL 8h bash "_bug_prediction.sh" $pid $bid $bid_data_dir "$revisions_weight" "$fixes_weight" "$authors_weight" "$time_range" "-1" false > "$log_file" 2>&1 &
              _register_background_process $!;
              _can_more_jobs_be_submitted;
            fi
          popd > /dev/null 2>&1
        done
      done
    done
  done
done

# It might be the case that there are still jobs running
_wait_for_jobs;

echo "All jobs submitted!"

# EOF

