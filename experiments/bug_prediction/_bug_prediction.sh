#!/bin/bash

#SCRIPT_DIR=$(cd `dirname $0` && pwd)
SCRIPT_DIR=`pwd`
source "$SCRIPT_DIR/../utils.sh" || exit 1

if _am_I_a_cluster; then
  module load apps/gcc/5.2/git/2.5
  module load apps/python/anaconda3-2.5.0
fi

export PYTHONHASHSEED=0 # set a seed to python's hash randomization

# -------------------------------------------------------- Envs & Args

# Check whether ANT_HOME is set
[ "$ANT_HOME" != "" ] || die "[ERROR] ANT_HOME is not set!"
# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "[ERROR] D4J_HOME is not set!"

USAGE="Usage: $0 <pid> <bid> <data dir> <revisions_weight> <fixes_weight> <authors_weight> <time range> <number commits, -1 to analyse all> <zip data? true | false>"
[ $# -eq 9 ] || die "$USAGE";
PID=$1
BID=$2
DATA_DIR=$3
REVISIONS_WEIGHT=$4
FIXES_WEIGHT=$5
AUTHORS_WEIGHT=$6
TIME_RANGE=$7
N_COMMITS=$8
ZIP_DATA=$9

SUM_FEATURES_WEIGHTS=$(echo "$REVISIONS_WEIGHT + $FIXES_WEIGHT + $AUTHORS_WEIGHT" | bc)
if [ $(echo "$SUM_FEATURES_WEIGHTS > 1.0" | bc -l) -eq 1 ] || [ $(echo "$SUM_FEATURES_WEIGHTS < 1.0" | bc -l) -eq 1 ]; then
  die "The sum of features weights must be 1.0";
fi

python_version=$(python -c 'import sys; print(".".join(map(str, sys.version_info[:2])))')
PYTHON_LIB_DIR="/home/$USER/.local/lib/python$python_version/site-packages"
if [ ! -d "$PYTHON_LIB_DIR" ]; then
  die "Library directory '$PYTHON_LIB_DIR' does not exist"
fi
export PYTHONPATH="$PYTHON_LIB_DIR:$PYTHONPATH"

export PATH="/home/$USER/.local/bin:$ANT_HOME/bin:$PATH"

LOCAL_TMP_DIR="/tmp/$USER/_bug-prediction-$PID-$BID-$$"
rm -rf "$LOCAL_TMP_DIR"; mkdir -p "$LOCAL_TMP_DIR"

LOCAL_DATA_DIR="$LOCAL_TMP_DIR/bug-prediction-data"
mkdir -p "$LOCAL_DATA_DIR"

# --------------------------------------------------------------- Main

echo "PID: $$"
hostname
python --version

python_short_version=$(python -c 'import sys; print("".join(map(str, sys.version_info[:2])))')
if [ "$python_short_version" -lt "34" ]; then # Schwa requires 3.4
  die "Schwa requires python >= 3.4"
fi

echo ""
echo "[INFO] Checkout $PID-${BID}b"

##
# for Chart we need to convert the local repository to GIT, as it
# is SVN.
if [ "$PID" == "Chart" ]; then
  # the following code is based on http://john.albin.net/git/convert-subversion-to-git
  echo "[INFO] Converting SVN into GIT"

  ## get list of authors
  echo "[INFO] Get list of authors from the SVN repository"
  trunk_full_path=$(cd "$D4J_HOME/project_repos/jfreechart" && pwd)
  trunk_full_path="$trunk_full_path/trunk"
  echo "[DEBUG] Trunk dir at '$trunk_full_path'"
  svn log -q file://$trunk_full_path | awk -F '|' '/^r/ {sub("^ ", "", $2); sub(" $", "", $2); print $2" = "$2" <"$2">"}' | sort -u > "$LOCAL_TMP_DIR/authors-transform.txt"
  if [ $? -ne 0 ] || [ ! -s "$LOCAL_TMP_DIR/authors-transform.txt" ]; then
    echo "[ERROR] Get list of authors failed!"
    rm -rf "$LOCAL_TMP_DIR"
    exit 1;
  fi
  cat "$LOCAL_TMP_DIR/authors-transform.txt"

  ## get buggy revision
  buggy_revision=$(grep "^$BID," "$D4J_HOME/framework/projects/Chart/commit-db" | cut -f2 -d',')
  echo "[DEBUG] Revision $buggy_revision for $PID-$BID"

  ## clone it
  echo "[INFO] Clonning it"
  git svn clone -r0:$buggy_revision file://$trunk_full_path --no-metadata -A "$LOCAL_TMP_DIR/authors-transform.txt" "$LOCAL_TMP_DIR/svn-repo/"
  if [ $? -ne 0 ]; then
    echo "[ERROR] git-svn failed!"
    rm -rf "$LOCAL_TMP_DIR"
    exit 1;
  fi

  ##
  # push repository to a local bare git repository
  echo "[INFO] Creating to a local bare git repository"
  git init --bare "$LOCAL_TMP_DIR/bare-repo/$PID-$BID.git"
  if [ $? -ne 0 ]; then
    echo "[ERROR] git init failed!"
    rm -rf "$LOCAL_TMP_DIR"
    exit 1;
  fi

  echo "[INFO] Symbolic-ref HEAD and GIT-SVN branch"
  pushd . > /dev/null 2>&1  
  cd "$LOCAL_TMP_DIR/bare-repo/$PID-$BID.git"
    git symbolic-ref HEAD refs/heads/git-svn
  popd > /dev/null 2>&1

  echo "[INFO] Pushing to local bare"
  pushd . > /dev/null 2>&1  
  cd "$LOCAL_TMP_DIR/svn-repo/"
    git remote add bare "$LOCAL_TMP_DIR/bare-repo/$PID-$BID.git"
    git config remote.bare.push 'refs/remotes/*:refs/heads/*'
    git push bare
  popd > /dev/null 2>&1

  ## rename "trunk" branch to "master"
  echo "[INFO] Renaming 'git-svn' branch to master"
  pushd . > /dev/null 2>&1  
  cd "$LOCAL_TMP_DIR/bare-repo/$PID-$BID.git"
    git branch -m git-svn master
  popd > /dev/null 2>&1

  ## clean up branches and tags
  echo "[INFO] Cleaning up branches and tags"
  pushd . > /dev/null 2>&1  
  cd "$LOCAL_TMP_DIR/bare-repo/$PID-$BID.git"
    git for-each-ref --format='%(refname)' refs/heads/tags | cut -d / -f 4 | while read ref; do
      git tag "$ref" "refs/heads/tags/$ref"
      git branch -D "tags/$ref"
    done
  popd > /dev/null 2>&1

  ## clone the local bare one
  echo "[INFO] Clonning the local bare repository"
  git clone "$LOCAL_TMP_DIR/bare-repo/$PID-$BID.git" "$LOCAL_TMP_DIR/git-repo/"
  pushd . > /dev/null 2>&1  
  cd "$LOCAL_TMP_DIR/git-repo/"
    # Using this command because the old copy was a git-svn clone
    git clean -dXn
  popd > /dev/null 2>&1

  ## clean up directories
  echo "[INFO] Cleanning up directories"
  rm -rf "$LOCAL_TMP_DIR/svn-repo"
  rm -rf "$LOCAL_TMP_DIR/bare-repo/$PID-$BID.git"
  #cp -Rv $LOCAL_TMP_DIR/git-repo/* "$LOCAL_TMP_DIR/"
  #rm -rf "$LOCAL_TMP_DIR/git-repo/"
  checkout_dir="$LOCAL_TMP_DIR/git-repo/"
else
  checkout_dir=$(_checkout $PID $BID "b");
  if [ $? -ne 0 ]; then
    echo "[ERROR] Checkout of the BUGGY version failed!"
    rm -rf "$checkout_dir" "$LOCAL_TMP_DIR"
    exit 1;
  fi
fi

echo ""
echo "[INFO] Mining $PID-${BID}b"

pushd . > /dev/null 2>&1
cd "$checkout_dir"

  ##
  # go back to the official buggy commit/revision

  if [ "$PID" != "Chart" ]; then
    commit_hash=$(grep "^$BID," "$D4J_HOME/framework/projects/$PID/commit-db" | cut -f2 -d',')
    git checkout "$commit_hash" || die "Checkout of commit '$commit_hash' has failed!"
  fi

  ##
  # execute bug prediction tool

  num_commits="$N_COMMITS"
  if [ "$N_COMMITS" -eq "-1" ]; then
    #num_commits=$(git rev-list --count HEAD) # only works on recent versions of git
    num_commits=$(git --no-pager log --pretty=oneline --abbrev-commit | wc -l)
  fi
  echo "[DEBUG] Number of commits to analyse: $num_commits"

  json_file="$LOCAL_DATA_DIR/$PID-${BID}b.json"
  json_pretty_print_file="$LOCAL_DATA_DIR/$PID-${BID}b-pretty-print.json"

  echo "[INFO] Executing the bug-prediction-tool"

  echo "commits: $num_commits # maximum commits" > ".schwa.yml"
  echo "features_weights: # sum must be 1" >> ".schwa.yml"
  echo "  revisions: $REVISIONS_WEIGHT" >> ".schwa.yml"
  echo "  fixes: $FIXES_WEIGHT" >> ".schwa.yml"
  echo "  authors: $AUTHORS_WEIGHT" >> ".schwa.yml"
  echo "time_range: $TIME_RANGE" >> ".schwa.yml"
  cat ".schwa.yml"
  schwa -s -j . > "$json_file" || die "Schwa failed!"

  if [ ! -s "$json_file" ]; then
    echo "[ERROR] there is not any '$json_file' or it is empty"

    popd > /dev/null 2>&1; rm -rf "$checkout_dir" "$LOCAL_TMP_DIR"
    die "[ERROR] there is not any '$json_file' or it is empty"
  fi

  # sometimes the .json contains python errors, remove those first
  sed -i '/^Illegal character /d' "$json_file"

  ##
  # pretty print the json file so that we can grep it

  echo "[INFO] Pretty pritting the json file"
  sed -i "s/'/\"/g" "$json_file" # convert ' into "
  python -m json.tool "$json_file" > "$json_pretty_print_file" || die "Pretty-print failed!"

  if [ ! -s "$json_pretty_print_file" ]; then
    echo "[ERROR] there is not any '$json_pretty_print_file' or it is empty"

    popd > /dev/null 2>&1; rm -rf "$checkout_dir" "$LOCAL_TMP_DIR"
    die "[ERROR] there is not any '$json_pretty_print_file' or it is empty"
  fi

  ##
  # create a ranking of .java files

  echo "[INFO] Creating a ranking file"
  ranking_file="$LOCAL_DATA_DIR/ranking.csv"
  echo "project_name,bug_id,revisions_weight,fixes_weight,authors_weight,time_range,java_file_path,class_name,prediction" > "$ranking_file"

  while read -r java_file_path; do
    echo "[DEBUG] java_file_path: $java_file_path"
    if [ ! -f "$java_file_path" ]; then
      # it means that the file existed in the history of the repository
      # but there is not such file anymore
      echo "[DEBUG] file '$java_file_path' not found!"
      continue;
    fi

    package_name=$(grep -E "^[[:blank:]]*package[[:blank:]]*(\w*\.)*\w*[[:blank:]]*;" "$java_file_path" | xargs | cut -f2 -d' ' | sed 's/;//' | tr -d $'\r')
    if [ "$package_name" != "" ]; then
      package_name="$package_name."
    fi

    python "$SCRIPT_DIR/get_classes_prediction_per_java_file.py" "$json_pretty_print_file" "$java_file_path" | while read -r class_name; do
      echo "$PID,$BID,$REVISIONS_WEIGHT,$FIXES_WEIGHT,$AUTHORS_WEIGHT,$TIME_RANGE,$java_file_path,$package_name$class_name" >> "$ranking_file"
    done
  done < <(grep "\"path\": \".*.java\"" "$json_pretty_print_file" | cut -f2 -d':' | sed 's/"//g' | sed 's/,//g')

  if [ ! -s "$ranking_file" ]; then
    echo "[ERROR] there is not any '$ranking_file' or it is empty"

    popd > /dev/null 2>&1; rm -rf "$checkout_dir" "$LOCAL_TMP_DIR"
    die "[ERROR] there is not any '$ranking_file'"
  else
    num_lines=$(wc -l "$ranking_file" | cut -f1 -d' ')
    if [ "$num_lines" -eq "1" ]; then
      echo "[ERROR] '$ranking_file' file only includes the header"

      popd > /dev/null 2>&1; rm -rf "$checkout_dir" "$LOCAL_TMP_DIR"
      die "[ERROR] '$ranking_file' file only includes the header"
    fi
  fi

  ##
  # sort (DESC) ranking by third column, i.e., probability of being faulty
  (head -n 1 "$ranking_file" && tail -n +2 "$ranking_file" | sort -t ',' -k 9,9rn) > "$ranking_file.sorted" || die "Sort ranking failed!"
  mv -f "$ranking_file.sorted" "$ranking_file"
  echo "[DEBUG] Ranking:"; cat "$ranking_file"
popd > /dev/null 2>&1

echo ""
echo "[INFO] Collect data & Clean up"

pushd . > /dev/null 2>&1
cd "$LOCAL_TMP_DIR/"

  if [ "$ZIP_DATA" == true ]; then
    echo "DONE!"

    # get log file so that it can also be in the .tar.bz2 file
    cp -f "$DATA_DIR/log.txt" "bug-prediction-data/" > /dev/null 2>&1

    zip_filename="$PID-$BID-bug-prediction-data.tar.bz2"
    if [ "$N_COMMITS" -ne "-1" ]; then
      zip_filename="$PID-$BID-$N_COMMITS-commits-bug-prediction-data.tar.bz2"
    fi

    tar -jcvf "$zip_filename" "bug-prediction-data/"
    if [ $? -ne 0 ]; then
      echo "[ERROR] It was not possible to compress directory '$LOCAL_TMP_DIR/bug-prediction-data/'!"

      echo "[INFO] Copying all files from local '$LOCAL_TMP_DIR/bug-prediction-data' to remote '$DATA_DIR' so that anyone can debug them"
      cp -Rv bug-prediction-data/* "$DATA_DIR/"
      if [ $? -ne 0 ]; then
        echo "[ERROR] It was not possible to copy all local data to '$DATA_DIR'"
      fi

      popd > /dev/null 2>&1
      rm -rf "$checkout_dir" "$LOCAL_TMP_DIR"
      exit 1;
    fi

    cp -v "$zip_filename" "$DATA_DIR/../"
    # remove remote directory, as everything went fine
    rm -rf "$DATA_DIR" > /dev/null 2>&1
  else
    cp -Rv bug-prediction-data/* "$DATA_DIR/"
    if [ $? -ne 0 ]; then
      echo "[ERROR] It was not possible to copy all local data to '$DATA_DIR'"

      popd > /dev/null 2>&1
      rm -rf "$checkout_dir" "$LOCAL_TMP_DIR"
      exit 1;
    fi

    echo "DONE!"
  fi
  
popd > /dev/null 2>&1

# do not leave anything behind
rm -rf "$checkout_dir" "$LOCAL_TMP_DIR" > /dev/null 2>&1

exit 0;

# EOF

