#!/bin/bash
#
# --------------------------------------------------------------------
# This script prints to a file ranking information about `modified
# classes`, i.e., classes that actually contain the bug.
#
# Usage:
# ranking_of_modified_classes.sh
#
# Requirements:
#   Execution of tools/get_tools.sh script.
# --------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether D4J_HOME exists
D4J_HOME="$SCRIPT_DIR/../../tools/defects4j"
if [ ! -d "$D4J_HOME" ]; then
  die "Could not find 'Defects4J' home directory! Did you run 'tools/get_tools.sh'?"
else
  export D4J_HOME="$D4J_HOME"
fi

USAGE="Usage: $0"
[ $# -eq 0 ] || die "$USAGE";

DATA_DIR="$SCRIPT_DIR/../../data"
if [ ! -d "$DATA_DIR" ]; then
  die "'$DATA_DIR' does not exist!"
fi

ALL_BUGGY_CLASSES_FILE="$SCRIPT_DIR/../../defects4J/all_buggy_classes.csv"
if [ ! -f "$ALL_BUGGY_CLASSES_FILE" ]; then
  die "'$ALL_BUGGY_CLASSES_FILE' file does not exist!";
fi

CSV="$SCRIPT_DIR/ranking_of_modified_classes.csv"
echo "project,bug_id,modified_class,probability_of_being_buggy,ranking_position,ranking_size,ranking_entropy" > "$CSV"

# --------------------------------------------------------------- Main

log10() {
  echo "l($1)/l(10)" | bc -l
}

entropy() {
  local USAGE="Usage: entropy <file with values>"
  if [ "$#" != 1 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local values_file="$1"
  if [ ! -s "$values_file" ]; then
    echo "[ERROR] There is no '$values_file' file or is empty!">&2
    return 1
  fi

  local entropy_value=0.0

  while read -r value; do
    entropy_value=$(echo "$entropy_value + ($value * $(log10 $value))" | bc -l)
  done < <(tail -n +2 "$values_file" | cut -f9 -d',')

  entropy_value=$(echo "$entropy_value * -1.0" | bc -l)
  echo "$entropy_value"

  return 0
}

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"

  # Get all bugs
  bids=$(cut -f1 -d',' $dir_project/commit-db)

  # Iterate over all bugs
  for bid in $bids; do
    echo "[DEBUG] $pid-$bid"

    bug_prediction_data_zip="$DATA_DIR/$pid/$bid/$pid-$bid-bug-prediction-data.tar.bz2"
    if [ ! -s "$bug_prediction_data_zip" ]; then
      echo "[WARN] There is no '$bug_prediction_data_zip' file or is empty."
      continue;
    fi

    ranking_file_tmp_file="/tmp/ranking_file_tmp_$USER-$pid-$bid-$$.csv"
    >"$ranking_file_tmp_file"

    tar -xf "$bug_prediction_data_zip" bug-prediction-data/ranking.csv -O > "$ranking_file_tmp_file"
    if [ $? -ne 0 ]; then
      rm -f "$ranking_file_tmp_file"
      echo "[ERROR] It was not possible to extract 'ranking.csv' from '$bug_prediction_data_zip'"
      continue;
    elif [ ! -s "$ranking_file_tmp_file" ]; then
      echo "[ERROR] There is not any '$ranking_file_tmp_file' file or is empty."
      continue;
    fi

    ranking_size=$(wc -l "$ranking_file_tmp_file" | cut -f1 -d' ')
    echo "[DEBUG] Ranking Size: '$ranking_size'"

    ranking_entropy=$(entropy "$ranking_file_tmp_file")
    if [ $? -ne 0 ]; then
      echo "[ERROR] Entropy function has failed."
      continue;
    fi
    echo "[DEBUG] Ranking Entropy: '$ranking_entropy'"

    while read -r line; do
      #file_path=$(echo "$line" | cut -f3 -d',')
      modified_class=$(echo "$line" | cut -f4 -d',')

      # Chart,1,0.0,0.7,0.3,0.1,source/org/jfree/chart/plot/Plot.java,org.jfree.chart.plot.Plot,0.1845078964592090745004762288
      #prediction=$(grep "^$pid,$bid,.*,.*,.*,.*,$file_path,$modified_class," "$ranking_file_tmp_file")
      prediction=$(grep "^$pid,$bid,.*,.*,.*,.*,.*,$modified_class," "$ranking_file_tmp_file" | head -n1)
      echo "[DEBUG] Prediction: '$prediction'"

      ranking_position=$(grep -n "$prediction" "$ranking_file_tmp_file" | cut -f1 -d':')
      ranking_position=$((ranking_position-1)) # '$ranking_file_tmp_file' has a header
      echo "[DEBUG] Ranking Position: '$ranking_position'"
      ranking_probability=$(echo "$prediction" | cut -f9 -d',')
      echo "[DEBUG] Ranking Probability: '$ranking_probability'"

      if [ -z "$ranking_position" ] || [ -z "$ranking_probability" ]; then
        die "[ERROR] some variables are empty or null ($ranking_position:$ranking_probability)!"
      fi

      echo "$pid,$bid,$modified_class,$ranking_probability,$ranking_position,$ranking_size,$ranking_entropy" >> "$CSV"
    done < <(grep "^$pid,$bid," "$ALL_BUGGY_CLASSES_FILE")
  done
done

echo "DONE!"

# EOF

