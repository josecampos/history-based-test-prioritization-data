#!/usr/bin/python

""" Script to parse a .json file produced by `schwa`. It prints to the
stdout all classes of a specific java file (one per line) and its
probability of being buggy. """

import argparse
import json

argparser = argparse.ArgumentParser()
argparser.add_argument('json', nargs=1)
argparser.add_argument('java', nargs=1)

def getClasses(element, classes_prediction):
  if element['type'] == "class":
    classes_prediction[element['name']] = element['prob']
  elif len(element['children']) == 0 and element['type'] == "file":
    classes_prediction[element['name'].replace(".java", "")] = element['prob']
  else:
    for child in element['children']:
      getClasses(child, classes_prediction)

if __name__ == '__main__':
  args = argparser.parse_args()

  with open(args.json[0], encoding='UTF-8') as data_file:
    data = json.loads(data_file.read())

  for java_file_element in data['children']:
    if java_file_element['path'] == args.java[0]:
      classes_prediction={}
      getClasses(java_file_element, classes_prediction)
      for clazz in classes_prediction:
        print(clazz + "," + classes_prediction[clazz])

# EOF

