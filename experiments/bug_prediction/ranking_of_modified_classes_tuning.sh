#!/bin/bash
#
# --------------------------------------------------------------------
# This script prints to a file ranking information about `modified
# classes`, i.e., classes that actually contain the bug.
#
# Usage:
# ranking_of_modified_classes_tuning.sh <DATA DIR>
#
# Parameters:
#   <DATA DIR>        Directory with the output of `bug_prediction_tuning.sh`
#                     script.
#
# --------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../../utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

USAGE="Usage: $0 <data dir>"
[ $# -eq 1 ] || die "$USAGE";
DATA_DIR=$1

if [ ! -d "$DATA_DIR" ]; then
  die "'$DATA_DIR' does not exist!"
fi

BUGS_STRATIFIED_FILE="$SCRIPT_DIR/../../defects4J/bugs.stratified.txt"
if [ ! -f "$BUGS_STRATIFIED_FILE" ]; then
  die "'$BUGS_STRATIFIED_FILE' file does not exit!";
fi

ALL_BUGGY_CLASSES_FILE="$SCRIPT_DIR/../../defects4J/all_buggy_classes.csv"
if [ ! -f "$ALL_BUGGY_CLASSES_FILE" ]; then
  die "'$ALL_BUGGY_CLASSES_FILE' file does not exist!";
fi

CSV="$SCRIPT_DIR/ranking_of_modified_classes_tuning.csv"
echo "configuration,project,bug_id,modified_class,probability_of_being_buggy,ranking_position,ranking_size" > "$CSV"

# --------------------------------------------------------------- Main

tail -n +2 "$BUGS_STRATIFIED_FILE" | while read -r project_bug; do
  pid=$(echo "$project_bug" | cut -f1 -d',')
  bid=$(echo "$project_bug" | cut -f2 -d',')

  # iterate over all (possible) configurations
  for revisions_weight in 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0; do
    for fixes_weight in 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0; do
      for authors_weight in 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0; do

        # if it's not a valid configuration do not even try to run it
        sum_features_weights=$(echo "$revisions_weight + $fixes_weight + $authors_weight" | bc)
        if [ $(echo "$sum_features_weights > 1.0" | bc -l) -eq 1 ] || [ $(echo "$sum_features_weights < 1.0" | bc -l) -eq 1 ]; then
          continue;
        fi
        
        conf_name="revisions_weight_$revisions_weight-fixes_weight_$fixes_weight-authors_weight_$authors_weight"
        ranking_file="$DATA_DIR/$pid/$bid/bug-prediction-data/tuning/$conf_name/ranking.csv"
        if [ ! -s "$ranking_file" ]; then
          continue;
        fi

        ranking_size=$(wc -l "$ranking_file" | cut -f1 -d' ')
        #echo "ranking_size: $ranking_size"

        grep "^$pid,$bid," "$ALL_BUGGY_CLASSES_FILE" | while read -r line; do
          file_path=$(echo "$line" | cut -f3 -d',')
          modified_class=$(echo "$line" | cut -f4 -d',')

          prediction=$(grep "^$file_path,$modified_class," "$ranking_file")

          ranking_position=$(grep -n "$prediction" | cut -f1 -d':')
          #echo "ranking_position: $ranking_position"
          ranking_probability=$(echo "$prediction" | cut -f3 -d',')
          #echo "ranking_probability: $ranking_probability"

          if [ -z "$ranking_position" ] || [ -z "$ranking_probability" ]; then
            die "[ERROR] some variables are empty or null ($ranking_position:$ranking_probability)!"
          fi

          echo "$conf_name,$pid,$bid,$modified_class,$ranking_probability,$ranking_position,$ranking_size" >> "$CSV"
        done
      done
    done
  done
done

echo "DONE!"

# EOF

