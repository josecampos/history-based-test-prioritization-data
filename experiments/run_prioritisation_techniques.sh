#!/bin/bash
#
# --------------------------------------------------------------------
# This script submits as many jobs as the number of projects * number
# of bugs. Each job runs Kanonizo with a pre-defined algorithm on each
# D4J bug. The outcome of running the prioritisation algorithm is
# written to:
#     ../data/_project_/_bug_id_/manual/_project_-_bug_id_-_algorithm_-manual-prioritisation-data.tar.bz2
#
# Usage:
# run_prioritisation_techniques.sh
#     [--input_dir <../data>]
#     [--output_dir <../data>]
#     [--bugs_to_run <path>]
#     [--runtime <1>]
#     [--algorithm <greedy,additionalgreedy,geneticalgorithm,random,randomsearch,schwa,marijan,elbaum,huang,cho>]
#     [--bug_prediction_type <default|best|n_commits>]
#     [--bug_prediction_n_commits_file <file>]
#     [--secondary_objective <greedy,additionalgreedy,random,constraint_solver,similarity>]
#     [--classes_per_group <0:5:10:25>]
#     [--force <true|false>]
#     [--help]
#
# Requirements:
#   Execution of tools/get_tools.sh script.
#   The execution of Kanonizo on each D4J bug:
#     - coverage must have been previously collected (see experiments/collect_coverage.sh script)
#     - bug-prediction data must have been previously collected (see defects4J/bug_prediction.sh script)
# --------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../experiments/utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether ANT_HOME exists
ANT_HOME="$SCRIPT_DIR/../tools/apache-ant"
if [ ! -d "$ANT_HOME" ]; then
  die "Could not find java 'apache-ant' directory! Did you run 'tools/get_tools.sh'?"
else
  export ANT_HOME="$ANT_HOME"
fi

# Check whether D4J_HOME exists
D4J_HOME="$SCRIPT_DIR/../tools/defects4j"
if [ ! -d "$D4J_HOME" ]; then
  die "Could not find 'Defects4J' home directory! Did you run 'tools/get_tools.sh'?"
else
  export D4J_HOME="$D4J_HOME"
fi

# Check whether MINION_EXE exists
MINION_EXE="$SCRIPT_DIR/../tools/minion"
if [ ! -s "$MINION_EXE" ]; then
  die "Could not find the executable of the constraint solver 'minion'! Did you run 'tools/get_tools.sh'?"
else
  export MINION_EXE="$MINION_EXE"
fi

# Check whether KANONIZO_JAR exists
KANONIZO_JAR="$SCRIPT_DIR/../tools/kanonizo.jar"
if [ ! -s "$KANONIZO_JAR" ]; then
  die "Could not find 'kanonizo.jar' file! Did you run 'tools/get_tools.sh'?"
else
  export KANONIZO_JAR="$KANONIZO_JAR"
fi

# Check whether JAVA_7_HOME exists
JAVA_7_HOME="$SCRIPT_DIR/../tools/jdk-7"
if [ ! -d "$JAVA_7_HOME" ]; then
  die "Could not find java '$JAVA_7_HOME' directory! Did you run 'tools/get_tools.sh'?"
else
  export JAVA_7_HOME="$JAVA_7_HOME"
fi

# Check whether JAVA_8_HOME exists
JAVA_8_HOME="$SCRIPT_DIR/../tools/jdk-8"
if [ ! -d "$JAVA_8_HOME" ]; then
  die "Could not find java '$JAVA_8_HOME' directory! Did you run 'tools/get_tools.sh'?"
else
  export JAVA_8_HOME="$JAVA_8_HOME"
fi

USAGE="Usage: $0 [--input_dir <$SCRIPT_DIR/../data>] [--output_dir <$SCRIPT_DIR/../data>] [--bugs_to_run <path>] [--runtime <1>] [--algorithm <greedy,additionalgreedy,geneticalgorithm,random,randomsearch,schwa,marijan,elbaum,huang,cho>] [--bug_prediction_type <default|best|n_commits>] [--bug_prediction_n_commits_file <file>] [--secondary_objective <greedy,additionalgreedy,random,constraint_solver,similarity>] [--classes_per_group <0:5:10:25>] [--force <true|false>] [--help]"
mod_of_two=$(expr $# % 2)
# a valid number of parameters is 1 in case of --help, or any number
# multiple of 2 (including 0, which means no parameters at all)
if [ $# -ne 1 ] && [ $mod_of_two -ne 0 ]; then
  die "$USAGE";
fi

INPUT_DATA_DIR="$SCRIPT_DIR/../data"
OUTPUT_DATA_DIR="$SCRIPT_DIR/../data"
RUNTIME="1"
ALGORITHM="greedy"
BUG_PREDICTION_TYPE="default"
BUG_PREDICTION_N_COMMITS_FILE="/dev/null"
SECONDARY_OBJECTIVE="greedy"
CLASSES_PER_GROUP="0:5:10:25"
FORCE=false
BUGS_TO_RUN_FILE="/dev/null"

while [[ "$1" = --* ]]; do
  OPTION=$1; shift
  case $OPTION in
    (--input_dir)
      INPUT_DATA_DIR=$1;
      shift;;
    (--output_dir)
      OUTPUT_DATA_DIR=$1;
      shift;;
    (--bugs_to_run)
      BUGS_TO_RUN_FILE=$1;
      shift;;
    (--runtime)
      RUNTIME=$1;
      shift;;
    (--algorithm)
      ALGORITHM=$1;
      shift;;
    (--bug_prediction_type)
      BUG_PREDICTION_TYPE=$1;
      shift;;
    (--bug_prediction_n_commits_file)
      BUG_PREDICTION_N_COMMITS_FILE=$1;
      shift;;
    (--secondary_objective)
      SECONDARY_OBJECTIVE=$1;
      shift;;
    (--classes_per_group)
      CLASSES_PER_GROUP=$1;
      shift;;
    (--force)
      FORCE=$1;
      shift;;
    (--help)
      echo "$USAGE"
      exit 0;;
    (*)
      die "$USAGE";;
  esac
done

# check whether input data directory exists
if [ ! -d "$INPUT_DATA_DIR" ]; then
  die "Directory '$INPUT_DATA_DIR' does not exist!"
fi

# check whether user has write permissions to output data directory
mkdir -p "$OUTPUT_DATA_DIR"
if [ $? -ne 0 ]; then
  echo "[ERROR] You might not have write permissions to create '$OUTPUT_DATA_DIR'!"
  continue
fi

TOOL="manual" # TODO adapt script in case of automatically generated test cases

# --------------------------------------------------------------- Main

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"

  # Get all bugs
  bids=$(cut -f1 -d',' $dir_project/commit-db)

  # Iterate over all bugs
  for bid in $bids; do
    n_commits=-1

    if [ -f "$BUGS_TO_RUN_FILE" ]; then
      if ! grep -q "^$pid,$bid$" "$BUGS_TO_RUN_FILE"; then
        continue
      fi
    fi

    input_root_dir="$INPUT_DATA_DIR/$pid/$bid"
    if [ ! -d "$input_root_dir" ]; then
      echo "[WARNING] '$input_root_dir' does not exist"
      continue
    fi

    output_root_dir="$OUTPUT_DATA_DIR/$pid/$bid"
    mkdir -p "$output_root_dir"
    if [ $? -ne 0 ]; then
      echo "[ERROR] You might not have write permissions to create '$output_root_dir'!"
      continue
    fi

    log_file_tmp="/tmp/log_file_tmp_$USER-$pid-$bid-$TOOL-$$.txt"
    >"$log_file_tmp"

    ##
    # have we got prioritisation-data?
    if [ "$ALGORITHM" == "schwa" ]; then
      if [ "$BUG_PREDICTION_TYPE" == "default" ]; then
        prioritisation_data_zip="$output_root_dir/$TOOL/$pid-$bid-$ALGORITHM-$SECONDARY_OBJECTIVE-$TOOL-prioritisation-data.tar.bz2"
      elif [ "$BUG_PREDICTION_TYPE" == "best" ]; then
        prioritisation_data_zip="$output_root_dir/$TOOL/$pid-$bid-best-$ALGORITHM-$SECONDARY_OBJECTIVE-$TOOL-prioritisation-data.tar.bz2"
      elif [ "$BUG_PREDICTION_TYPE" == "n_commits" ]; then
        if [ ! -f "$BUG_PREDICTION_N_COMMITS_FILE" ]; then
          rm -f "$log_file_tmp"
          die "File '$BUG_PREDICTION_N_COMMITS_FILE' does not exist"
        fi

        if ! grep -q "^$pid,$bid," "$BUG_PREDICTION_N_COMMITS_FILE"; then
          # do not run pid-bid
          rm -f "$log_file_tmp"
          continue
        fi

        n_commits=$(grep "^$pid,$bid," "$BUG_PREDICTION_N_COMMITS_FILE" | cut -f3 -d',')
        if [ "$n_commits" -eq "0" ]; then
          # do not run pid-bid with 0 commits to analyse
          rm -f "$log_file_tmp"
          continue
        fi

        prioritisation_data_zip="$output_root_dir/$TOOL/$pid-$bid-$n_commits-commits-$ALGORITHM-$SECONDARY_OBJECTIVE-$TOOL-prioritisation-data.tar.bz2"
      else
        rm -f "$log_file_tmp"
        die "Bug prediction type '$BUG_PREDICTION_TYPE' not known"
      fi
    else
      prioritisation_data_zip="$output_root_dir/$TOOL/$pid-$bid-$ALGORITHM-$TOOL-prioritisation-data.tar.bz2"
    fi
    if [[ $FORCE == false ]]; then
      if [ -s "$prioritisation_data_zip" ]; then
        tar -xf "$prioritisation_data_zip" prioritisation-data/log.txt -O > "$log_file_tmp"
        if [ $? -ne 0 ]; then
          echo "[ERROR] It was not possible to extract 'log.txt' from '$prioritisation_data_zip', therefore a sanity-check on $pid-$bid could not be performed."
        else
          if grep -q "^DONE\!$" "$log_file_tmp" && ! grep -q " No space left on device$" "$log_file_tmp"; then
            # skip pid-bid if it has completed successfully
            rm -f "$log_file_tmp"
            continue;
          fi
        fi

        rm -f "$log_file_tmp"
      fi
    fi
    rm -f "$prioritisation_data_zip"

    ##
    # have we got coverage-data?
    coverage_data_zip="$input_root_dir/$TOOL/$pid-$bid-$TOOL-coverage-data.tar.bz2"
    if [ -s "$coverage_data_zip" ]; then
      tar -xf "$coverage_data_zip" coverage-data/log.txt -O > "$log_file_tmp"
      if [ $? -ne 0 ]; then
        rm -f "$log_file_tmp"
        echo "[ERROR] It was not possible to extract 'log.txt' from '$coverage_data_zip', therefore a sanity-check on $pid-$bid could not be performed."
        continue;
      fi

      # skip pid-bid if it has not completed successfully
      if ! grep -q "^DONE\!$" "$log_file_tmp"; then
        rm -f "$log_file_tmp"
        echo "[ERROR] Coverage-analysis of $pid-$bid has failed!"
        continue;
      fi

      rm -f "$log_file_tmp"
    else
      echo "[ERROR] No coverage data for $pid-$bid!"
      continue;
    fi

    ##
    # have we got bug-prediction-data?
    if [ "$BUG_PREDICTION_TYPE" == "default" ]; then
      bug_prediction_data_zip="$input_root_dir/$pid-$bid-bug-prediction-data.tar.bz2"
    elif [ "$BUG_PREDICTION_TYPE" == "best" ]; then
      bug_prediction_data_zip="$input_root_dir/$pid-$bid-perfect-bug-prediction-data.tar.bz2"
    elif [ "$BUG_PREDICTION_TYPE" == "n_commits" ]; then
      bug_prediction_data_zip="$input_root_dir/$pid-$bid-$n_commits-commits-bug-prediction-data.tar.bz2"
    else
      die "Bug prediction type '$BUG_PREDICTION_TYPE' not known"
    fi
    if [ "$ALGORITHM" == "schwa" ]; then
      if [ -s "$bug_prediction_data_zip" ]; then
        tar -xf "$bug_prediction_data_zip" bug-prediction-data/log.txt -O > "$log_file_tmp"
        if [ $? -ne 0 ]; then
          rm -f "$log_file_tmp"
          echo "[ERROR] It was not possible to extract 'log.txt' from '$bug_prediction_data_zip', therefore a sanity-check on $pid-$bid could not be performed."
          continue;
        fi

        # skip pid-bid if it has not completed successfully
        if ! grep -q "^DONE\!$" "$log_file_tmp"; then
          rm -f "$log_file_tmp"
          echo "[ERROR] Bug-prediction analysis of $pid-$bid has failed!"
          continue;
        fi

        rm -f "$log_file_tmp"
      else
        echo "[ERROR] No bug-prediction data for $pid-$bid!"
        continue;
      fi
    fi

    ##
    # have we got test evolution data?
    test_evolution_data_zip="$input_root_dir/$TOOL/$pid-$bid-$TOOL-test-evolution-data.tar.bz2"
    if [ "$ALGORITHM" == "marijan" ] || [ "$ALGORITHM" == "elbaum" ] || [ "$ALGORITHM" == "huang" ] || [ "$ALGORITHM" == "cho" ]; then
      if [ -s "$test_evolution_data_zip" ]; then
        tar -xf "$test_evolution_data_zip" test-evolution-data/log.txt -O > "$log_file_tmp"
        if [ $? -ne 0 ]; then
          rm -f "$log_file_tmp"
          echo "[ERROR] It was not possible to extract 'log.txt' from '$test_evolution_data_zip', therefore a sanity-check on $pid-$bid could not be performed."
          continue;
        fi

        # skip pid-bid if it has not completed successfully
        if ! grep -q "^DONE\!$" "$log_file_tmp" || grep -q " No space left on device$" "$log_file_tmp"; then
          rm -f "$log_file_tmp"
          echo "[ERROR] Test-evolution analysis of $pid-$bid has failed!"
          continue;
        fi

        rm -f "$log_file_tmp"
      else
        echo "[ERROR] No test-evolution data for $pid-$bid!"
        continue;
      fi
    fi

    prioritisation_data_dir="$output_root_dir/$TOOL/prioritisation-data-$ALGORITHM-$SECONDARY_OBJECTIVE-$$"
    rm -rf "$prioritisation_data_dir"
    if [ $? -ne 0 ]; then
      echo "[ERROR] You might not have write permissions to remove '$prioritisation_data_dir'!"
      continue
    fi
    mkdir -p "$prioritisation_data_dir"
    if [ $? -ne 0 ]; then
      echo "[ERROR] You might not have write permissions to create '$prioritisation_data_dir'!"
      continue
    fi

    echo "$pid-$bid"
    log_file="$prioritisation_data_dir/log.txt"
    echo "$pid-$bid" > "$log_file"
    if [ $? -ne 0 ]; then
      echo "[ERROR] You might not have write permissions to write to '$log_file'!"
      continue
    fi

    pushd . > /dev/null 2>&1
    cd "$SCRIPT_DIR"
      if _am_I_a_cluster; then
        qsub -V -N "_$bid-$pid-run_prioritisation_techniques" -l h_rt=$RUNTIME:00:00 -l rmem=4G -e "$log_file" -o "$log_file" -j y "_run_prioritisation_techniques.sh" $pid $bid $TOOL $prioritisation_data_dir $coverage_data_zip $bug_prediction_data_zip $test_evolution_data_zip $ALGORITHM $SECONDARY_OBJECTIVE $CLASSES_PER_GROUP $BUG_PREDICTION_TYPE $n_commits;
      else
        timeout --signal=KILL "${RUNTIME}h" bash "_run_prioritisation_techniques.sh" $pid $bid $TOOL $prioritisation_data_dir $coverage_data_zip $bug_prediction_data_zip $test_evolution_data_zip $ALGORITHM $SECONDARY_OBJECTIVE $CLASSES_PER_GROUP $BUG_PREDICTION_TYPE $n_commits > "$log_file" 2>&1 &
        _register_background_process $!;
        _can_more_jobs_be_submitted;
      fi
    popd > /dev/null 2>&1

  done
done

# It might be the case that there are still jobs running
_wait_for_jobs;

echo "All jobs submitted!"

# EOF

