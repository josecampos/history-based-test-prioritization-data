#!/bin/bash
#
# --------------------------------------------------------------------
# This script submits as many jobs as the number of projects * number
# of bugs. Each job runs Kanonizo on the buggy version of a particular
# D4J's project-bug and collect the coverage of all manually-written
# test cases. The collected coverage is written to:
#     ../data/_project_/_bug_id_/manual/_project_-_bug_id_-manual-coverage-data.tar.bz2
# Note: each job also checks whether the list of failing test cases
# reported by Kanonizo is the same as reported by D4J.
#
# Usage:
# collect_coverage.sh
#
# Requirements:
#   Execution of tools/get_tools.sh script.
# --------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../experiments/utils.sh" || exit 1

# -------------------------------------------------------- Envs & Args

# Check whether ANT_HOME exists
ANT_HOME="$SCRIPT_DIR/../tools/apache-ant"
if [ ! -d "$ANT_HOME" ]; then
  die "Could not find java 'apache-ant' directory! Did you run 'tools/get_tools.sh'?"
else
  export ANT_HOME="$ANT_HOME"
fi

# Check whether D4J_HOME exists
D4J_HOME="$SCRIPT_DIR/../tools/defects4j"
if [ ! -d "$D4J_HOME" ]; then
  die "Could not find 'Defects4J' home directory! Did you run 'tools/get_tools.sh'?"
else
  export D4J_HOME="$D4J_HOME"
fi

# Check whether KANONIZO_JAR exists
KANONIZO_JAR="$SCRIPT_DIR/../tools/kanonizo.jar"
if [ ! -s "$KANONIZO_JAR" ]; then
  die "Could not find 'kanonizo.jar' file! Did you run 'tools/get_tools.sh'?"
else
  export KANONIZO_JAR="$KANONIZO_JAR"
fi

# Check whether JAVA_HOME exists
JAVA_HOME="$SCRIPT_DIR/../tools/jdk-8"
if [ ! -d "$JAVA_HOME" ]; then
  die "Could not find java '$JAVA_HOME' directory! Did you run 'tools/get_tools.sh'?"
else
  export JAVA_HOME="$JAVA_HOME"
fi

USAGE="Usage: $0"
[ $# -eq 0 ] || die "$USAGE";

DATA_DIR="$SCRIPT_DIR/../data"
if [ ! -d "$DATA_DIR" ]; then
  die "Directory '$DATA_DIR' does not exist!"
fi

TOOL="manual" # TODO adapt script in case of automatically generated test cases

# --------------------------------------------------------------- Main

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"

  # Get all bugs
  bids=$(cut -f1 -d',' $dir_project/commit-db)

  # Iterate over all bugs
  for bid in $bids; do

    root_dir="$DATA_DIR/$pid/$bid/$TOOL"
    coverage_data_zip="$root_dir/$pid-$bid-$TOOL-coverage-data.tar.bz2"

    if [ -s "$coverage_data_zip" ]; then
      log_file_tmp="/tmp/log_file_tmp_$pid-$bid-$TOOL-$$.txt"
      tar -xf "$coverage_data_zip" coverage-data/log.txt -O > "$log_file_tmp"
      if [ $? -ne 0 ]; then
        echo "[ERROR] It was not possible to extract 'log.txt' from '$coverage_data_zip', therefore a sanity-check on $pid-$bid could not be performed."
        continue;
      fi

      # skip pid-bid if it has completed successfully
      if grep -q "^DONE\!$" "$log_file_tmp" && ! grep -q " No space left on device$" "$log_file_tmp"; then
        rm -f "$log_file_tmp"
        continue;
      fi

      rm -f "$log_file_tmp"
    fi

    coverage_data_dir="$root_dir/coverage-data"
    rm -rf "$coverage_data_dir"; mkdir -p "$coverage_data_dir"
    log_file="$coverage_data_dir/log.txt"

    echo "$pid-$bid"
    echo "$pid-$bid" > "$log_file"

    pushd . > /dev/null 2>&1
    cd "$SCRIPT_DIR"
      if _am_I_a_cluster; then
        qsub -V -N "_$bid-$pid-collect_coverage" -l h_rt=04:00:00 -l rmem=4G -e "$log_file" -o "$log_file" -j y "_collect_coverage.sh" $pid $bid $TOOL $coverage_data_dir;
      else
        timeout --signal=KILL 4h bash "_collect_coverage.sh" $pid $bid $TOOL $coverage_data_dir > "$log_file" 2>&1 &
        _register_background_process $!;
        _can_more_jobs_be_submitted;
      fi
    popd > /dev/null 2>&1
  done
done

# It might be the case that there are still jobs running
_wait_for_jobs;

echo "All jobs submitted!"

# EOF

