#!/bin/bash
#
# --------------------------------------------------------------------
# This script downloads and sets up the following tools:
#   - Defects4J
#   - and several other tools like Java, Ant, Maven, and Kanonizo
#
# Usage:
# get_tools.sh
#
# --------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../experiments/utils.sh" || exit 1

# --------------------------------------------------------------- Deps

# Check whether 'git' is available
if ! git --version > /dev/null 2>&1; then
  die "[ERROR] Could not find 'git' to, e.g., clone Defects4J repository. Please install 'git' and re-run the script."
fi

# Check whether 'wget' is available
if ! wget --version > /dev/null 2>&1; then
  die "[ERROR] Could not find 'wget' to download all dependencies. Please install 'wget' and re-run the script."
fi

# Check whether 'curl' is available
if ! curl --version > /dev/null 2>&1; then
  die "[ERROR] Could not find 'curl' to download all dependencies. Please install 'curl' and re-run the script."
fi

# Check whether 'tar' is available
if ! tar --version > /dev/null 2>&1; then
  die "[ERROR] Could not find 'tar' to download all dependencies. Please install 'tar' and re-run the script."
fi

# Check whether 'gunzip' is available
if ! gunzip --version > /dev/null 2>&1; then
  die "[ERROR] Could not find 'gunzip' to download all dependencies. Please install 'gunzip' and re-run the script."
fi

# Check whether python 'pip' is available
if ! pip --version > /dev/null 2>&1; then
  die "[ERROR] Could not find 'pip' command. Please install python 'pip' >= 3.4 and re-run the script."
fi

# --------------------------------------------------------------- Main

OS_NAME=$(uname -s | tr "[:upper:]" "[:lower:]")
OS_ARCH=$(uname -m | tr "[:upper:]" "[:lower:]")

##
# Download JDK...
##

echo ""
echo "Setting up JDK-8..."

if [[ $OS_NAME == *"linux"* ]]; then

  JDK_OS="linux"
  if [ "$OS_ARCH" == "x86_64" ] || [ "$OS_ARCH" == "amd64" ]; then
    JDK_ARCH="x64"
  else
    JDK_ARCH="i586"
  fi

  JDK_VERSION="8u172"
  JDK_BUILD_VERSION="$JDK_VERSION-b11"
  JDK_FILE="jdk-$JDK_VERSION-$JDK_OS-$JDK_ARCH.tar.gz"
  JDK_HASH="a58eab1ec242421181065cdc37240b08"
  JDK_DIR="$SCRIPT_DIR/jdk1.8.0_172"
  JDK_URL="http://download.oracle.com/otn-pub/java/jdk/$JDK_BUILD_VERSION/$JDK_HASH/$JDK_FILE"

  if [ ! -d "$SCRIPT_DIR/jdk-8" ]; then
    # remove any previous file
    rm -f "$SCRIPT_DIR/$JDK_FILE"

    wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" -np -nv "$JDK_URL" -O "$SCRIPT_DIR/$JDK_FILE"
    if [ $? -ne 0 ] || [ ! -s "$SCRIPT_DIR/$JDK_FILE" ]; then
      die "[ERROR] Download of '$JDK_URL' has failed!"
    fi

    pushd . > /dev/null 2>&1
    cd "$SCRIPT_DIR"
      tar -xvzf "$JDK_FILE" # extract it
      if [ $? -ne 0 ] || [ ! -d "$JDK_DIR" ]; then
        die "[ERROR] Extraction of '$SCRIPT_DIR/$JDK_FILE' has failed!"
      fi

      mv -f "$JDK_DIR" "jdk-8"
    popd > /dev/null 2>&1
  fi
else
  die "[WARN] All scripts have been developed and tested on Linux, and as we are not sure they will work on other OS, we can continue running this script."
fi

##
# Download Ant
##

echo ""
echo "Setting up Ant..."

ANT_VERSION="1.10.2"
ANT_FILE="apache-ant-$ANT_VERSION-bin.tar.bz2"
ANT_URL="https://archive.apache.org/dist/ant/binaries/$ANT_FILE"
ANT_DIR="$SCRIPT_DIR/apache-ant-$ANT_VERSION"

# remove any previous file or directory
rm -f "$SCRIPT_DIR/$ANT_FILE"
rm -rf "$SCRIPT_DIR/apache-ant"

wget -np -nv "$ANT_URL" -O "$SCRIPT_DIR/$ANT_FILE"
if [ $? -ne 0 ] || [ ! -s "$SCRIPT_DIR/$ANT_FILE" ]; then
  die "[ERROR] Download of '$ANT_URL' has failed!"
fi

pushd . > /dev/null 2>&1
cd "$SCRIPT_DIR"
  tar -jxvf "$ANT_FILE" # extract it
  if [ $? -ne 0 ] || [ ! -d "$ANT_DIR" ]; then
    die "[ERROR] Extraction of '$SCRIPT_DIR/$ANT_FILE' has failed!"
  fi

  mv -f "$ANT_DIR" "apache-ant"
popd > /dev/null 2>&1

##
# Download Defects4J
##

echo ""
echo "Setting up Defects4J..."

# remove any previous 'defects4j' directory
rm -rf "$SCRIPT_DIR/defects4j"

git clone https://github.com/jose/defects4j.git "$SCRIPT_DIR/defects4j"
if [ $? -ne 0 ] || [ ! -d "$SCRIPT_DIR/defects4j" ]; then
  die "[ERROR] Clone of 'Defects4J' has failed!"
fi

pushd . > /dev/null 2>&1
cd "$SCRIPT_DIR/defects4j"
  echo "* merge=union" > .gitattributes

  # base commit
  git checkout 6fd5208a9714a0d27571a0154313844d218fd792 || die "[ERROR] Checkout of base commit '6fd5208a9714a0d27571a0154313844d218fd792' has failed!"

  # merge dir.bin.tests branch
  git merge --no-edit origin/dir.bin.tests || die "[ERROR] Merge of 'dir.bin.tests' branch has failed!"

  # merge test_evolution branch
  git merge --no-edit origin/test_evolution || die "[ERROR] Merge of 'test_evolution' branch has failed!"

  # merge fault-localization branch
  git merge --no-edit origin/fault-localization || die "[ERROR] Merge of 'fault-localization' branch has failed!"

  # init Defects4J, i.e., get repos, major, etc.
  bash init.sh || die "[ERROR] Init script of 'Defects4J' has failed!"
popd > /dev/null 2>&1

##
# Download Maven
##

echo ""
echo "Setting up Maven..."

MVN_VERSION="3.5.3"
MVN_FILE="apache-maven-$MVN_VERSION-bin.tar.gz"
MVN_URL="http://mirror.ox.ac.uk/sites/rsync.apache.org/maven/maven-3/$MVN_VERSION/binaries/$MVN_FILE"
MVN_DIR="$SCRIPT_DIR/apache-maven-$MVN_VERSION"

# remove any previous file or directory
rm -f "$SCRIPT_DIR/$MVN_FILE"
rm -rf "$SCRIPT_DIR/apache-maven"

wget -np -nv "$MVN_URL" -O "$SCRIPT_DIR/$MVN_FILE"
if [ $? -ne 0 ] || [ ! -s "$SCRIPT_DIR/$MVN_FILE" ]; then
  die "[ERROR] Download of '$MVN_URL' failed!"
fi

pushd . > /dev/null 2>&1
cd "$SCRIPT_DIR"
  tar -xvzf "$MVN_FILE" # extract it
  if [ $? -ne 0 ] || [ ! -d "$MVN_DIR" ]; then
    die "[ERROR] Extraction of '$SCRIPT_DIR/$MVN_FILE' failed!"
  fi

  mv -f "$MVN_DIR" "apache-maven"
popd > /dev/null 2>&1

##
# Download Kanonizo
##

echo ""
echo "Setting up Kanonizo..."

# remove any previous 'kanonizo' directory
rm -rf "$SCRIPT_DIR/kanonizo"

git clone https://github.com/kanonizo/kanonizo.git "$SCRIPT_DIR/kanonizo"
if [ $? -ne 0 ] || [ ! -d "$SCRIPT_DIR/kanonizo" ]; then
  die "[ERROR] Clone of 'Kanonizo' has failed!"
fi

KANONIZO_JAR="$SCRIPT_DIR/kanonizo.jar"

pushd . > /dev/null 2>&1
cd "$SCRIPT_DIR/kanonizo"
  export PATH="$SCRIPT_DIR/jdk-8/bin:$PATH"

  git checkout 185c2f302a5c8b7b34079f467765aa5ee4ea0f17 || die "[ERROR] Checkout of commit '185c2f302a5c8b7b34079f467765aa5ee4ea0f17' has failed!"

  ##
  # get 'scythe' (i.e., instrumentation)
  git clone --depth=1 https://github.com/thomasdeanwhite/scythe.git "$SCRIPT_DIR/kanonizo/scythe"
  if [ $? -ne 0 ] || [ ! -d "$SCRIPT_DIR/kanonizo/scythe" ]; then
    die "[ERROR] Clone of 'scythe' has failed!"
  fi

  pushd . > /dev/null 2>&1
  cd "$SCRIPT_DIR/kanonizo/scythe"
    git fetch --all || die "[ERROR] Fetch of 'scythe' has failed!"
    git branch dev || die "[ERROR] Creating of 'dev' branch has failed!"
    git checkout dev || die "[ERROR] Checkout of 'dev' branch has failed!"
    git pull origin dev || die "[ERROR] Pull of 'dev' branch has failed!"
    sh "$SCRIPT_DIR/apache-maven/bin/mvn" clean install -DskipTests=true || die "[ERROR] Compilation of 'scythe' has failed!"
  pushd . > /dev/null 2>&1

  # compile Kanonizo
  pushd . > /dev/null 2>&1
  cd "$SCRIPT_DIR/kanonizo/client"
    sh "$SCRIPT_DIR/apache-maven/bin/mvn" clean package -DskipTests=true || die "[ERROR] Compilation of 'Kanonizo' has failed!"

    LOCAL_KANONIZO_JAR="target/kanonizo.jar"
    if [ ! -s "$LOCAL_KANONIZO_JAR" ]; then
      popd > /dev/null 2>&1; popd > /dev/null 2>&1
      die "[ERROR] '$LOCAL_KANONIZO_JAR' does not exist or are empty!"
    fi

    mv "$LOCAL_KANONIZO_JAR" "$KANONIZO_JAR"
  pushd . > /dev/null 2>&1
popd > /dev/null 2>&1

##
# Schwa
##

echo ""
echo "Setting up Schwa..."

SCHWA_DIR="$SCRIPT_DIR/schwa"
rm -rf "$SCHWA_DIR"

python_version=$(python -c 'import sys; print("".join(map(str, sys.version_info[:2])))')
if [ "$python_version" -lt "34" ]; then # Schwa requires 3.4
  die "[ERROR] Schwa requires python >= 3.4"
fi

git clone https://github.com/jose/schwa.git "$SCHWA_DIR"
if [ $? -ne 0 ] || [ ! -d "$SCHWA_DIR" ]; then
  die "[ERROR] Clone of Schwa's has failed!"
fi

if _am_I_a_cluster; then
  module load apps/python/anaconda3-2.5.0
  if [ $? -ne 0 ]; then
    die "[ERROR] Failed to load module python module!"
  fi
fi

pushd . > /dev/null 2>&1
cd "$SCHWA_DIR"
  if [ ! -s "requirements.txt" ]; then
    die "[ERROR] '$SCHWA_DIR/requirements.txt' does not exist!"
  fi

  pip install --target=$SCHWA_DIR/.reqs -r requirements.txt
  if [ $? -ne 0 ]; then
    die "[ERROR] Failed to install Schwa's dependencies!"
  fi

  python setup.py install --user
  if [ $? -ne 0 ]; then
    die "[ERROR] Installation of Schwa has failed!"
  fi
popd > /dev/null 2>&1

##
# Download Minion
##

echo ""
echo "Setting up Minion..." # some test case prioritization may need to use an external constraint solver

MINION_TAR_FILE="minion.tar"
MINION_GZ_FILE="$MINION_GZ_FILE.gz"
MINION_EXEC="minion"
MINION_VERSION="1.8"
MINION_DIR="minion-$MINION_VERSION"
MINION_URL="https://constraintmodelling.org/?smd_process_download=1&download_id=226"

# remove any previous file or directory
rm -f "$SCRIPT_DIR/$MINION_TAR_FILE" "$SCRIPT_DIR/$MINION_GZ_FILE" "$SCRIPT_DIR/$MINION_EXEC"
rm -rf "$SCRIPT_DIR/$MINION_DIR"

curl -o "$MINION_GZ_FILE" -J -L "$MINION_URL"
if [ $? -ne 0 ] || [ ! -s "$SCRIPT_DIR/$MINION_GZ_FILE" ]; then
  die "[ERROR] Download of minion from $MINION_URL has failed!"
fi

pushd . > /dev/null 2>&1
cd "$SCRIPT_DIR"
  # undo .gz
  gunzip "$MINION_GZ_FILE" || die "[ERROR] gunzip of '$MINION_GZ_FILE' file has failed!"
  # undo .tar
  tar -xvf "$MINION_TAR_FILE" || die "[ERROR] tar of '$MINION_TAR_FILE' file has failed!"

  if [ ! -d "$MINION_DIR" ]; then
    die "[ERROR] '$SCRIPT_DIR/$MINION_DIR' does not exist!"
  fi
  if [ ! -f "$MINION_DIR/bin/minion" ]; then
    die "[ERROR] '$MINION_DIR/bin/minion' does not exist!"
  fi

  mv -f "$MINION_DIR/bin/minion" "$SCRIPT_DIR/$MINION_EXEC"
popd > /dev/null 2>&1

echo ""
echo "All tools have been successfully prepared."

# EOF

